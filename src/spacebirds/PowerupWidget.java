package spacebirds;

import processing.core.PImage;
import spacebirds.PlayerObject.PowerUps;
import spacebirds.base.Scene;
import spacebirds.widgets.ImageWidget;

/**
 * Image widget displaying a powerup.
 * 
 */

public class PowerupWidget extends ImageWidget {
	
	private PowerUps powerup;
	
	public PowerupWidget(Scene scene) {
		super(scene, "powerup_none.png");
		this.setPowerup(PowerUps.NONE);
	}
	
	public void setPowerup(PowerUps powerup) {
		PImage powerupImage = null;
		switch(powerup) {
		case NONE:
			powerupImage = p.assetManager().loadImage("powerup_none.png", 0, 0);
			break;
		case FLASH:
			powerupImage = p.assetManager().loadImage("powerup_blitz.png", 0, 0);
			break;
		case GHOST:
			powerupImage = p.assetManager().loadImage("powerup_ghost.png", 0, 0);
			break;
		case MARIO:
			powerupImage = p.assetManager().loadImage("powerup_mario.png", 0, 0);
			break;
		case CATAPULT: 
			powerupImage = p.assetManager().loadImage("powerup_catapult.png", 0, 0);
			break;
		}
		this.setImage(powerupImage);
		this.powerup = powerup;
	}
	
	public PowerUps getPowerup() {
		return powerup;
	}
	
}
