package spacebirds.widgets;

import processing.core.PImage;
import spacebirds.base.*;

/**
 * Displays a part of a gradient image according to the current
 * progress between 0 and 1.
 */
public class GaugeWidget extends Widget {

	private PImage gauge;
	private float progress = 0;
	
	public GaugeWidget(Scene scene) {
		super(scene);
		gauge = p.assetManager().loadImage("gauge.png", 0, 0);
		this.setBackground(0, 0, 0, 128);
	}

	public float getProgress() {
		return progress;
	}
	public GaugeWidget setProgress(float progress) {
		this.progress = progress;
		return this;
	}
	
	@Override
	public void draw() {
		super.draw();
		p.image(gauge, x, y, gauge.width * progress, gauge.height,
				0, 0, (int) (gauge.width * progress), gauge.height);
	}
	
	@Override
	public float getInnerWidth() {
		return gauge.width;
	}

	@Override
	public float getInnerHeight() {
		return gauge.height;
	}

}
