package spacebirds.widgets;

import java.util.HashMap;

import processing.core.*;
import spacebirds.base.Scene;
import spacebirds.base.Widget;

/**
 * Widget displaying text.
 * This does'nt use the processing text function but
 * loads all characters using an image and a mapping string.
 * On draw, this draws all images for its label after another.
 */
public class EightBitTextWidget extends Widget {
	
	public static final String FONT_MAPPING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+-=`~{}[]|\\:;\"\'<>,.?/";
	public static final String FONT_ASSET = "font.png";
	public static final int CHAR_WIDTH = 5;
	public static final int CHAR_HEIGHT = 8;
	
	private float fontScaling = 1.0f;
	private String label;
	private HashMap<Character, PImage> font;
	
	/**
	 * create the font mapping
	 */
	private void loadFont() {
		this.font = new HashMap<Character, PImage>(FONT_MAPPING.length());
		PImage fontImage = p.assetManager().loadImage(FONT_ASSET, 0, 0);
		p.noSmooth();		
		for(int i = 0; i < FONT_MAPPING.length(); ++i) {
			Character c = FONT_MAPPING.charAt(i);
			PImage cImage = fontImage.get(i*CHAR_WIDTH, 0, CHAR_WIDTH, CHAR_HEIGHT);
			//cImage.resize(0, 32);
			font.put(c, cImage);
		}
	}
	
	public EightBitTextWidget(Scene scene, String msg) {
		super(scene);
		this.label = msg;
		loadFont();
	}
	
	public String getLabel() { return label; }
	public EightBitTextWidget setLabel(String label) {
		this.label = label;
		return this;
	}
	
	public void draw() {
		super.draw();
		p.noSmooth();
		
		int charWidth = getCharacterWidth();
		int charHeight = (int)getInnerHeight();
		int charSpacing = getCharacterSpacing();
		
		for(int i = 0; i < label.length(); ++i) {
			char c = label.charAt(i);
			if(Character.isWhitespace(c)) {
				continue;
			} else {
				PImage cImage = this.font.get(c);
				if(cImage != null) {
					p.image(cImage, x + i * (charWidth + charSpacing), y, charWidth, charHeight);
				}
			}
		}
	}
	/**
	 * change the size of the font (pixels)
	 */
	public EightBitTextWidget setFontSize(int size) {
		fontScaling = (float)size / CHAR_HEIGHT;
		return this;
	}
	/**
	 * maximum width of one character.
	 */
	public int getCharacterWidth() {
		return (int)(fontScaling * CHAR_WIDTH + 0.5);
	}
	/**
	 * Number of pixels skipped between characters.
	 */
	public int getCharacterSpacing() {
		return getCharacterWidth() >> 2;
	}

	@Override
	public float getInnerWidth() {
		return (getCharacterWidth() + getCharacterSpacing()) * label.length();
	}

	@Override
	public float getInnerHeight() {
		return (int)(fontScaling * CHAR_HEIGHT + 0.5);
	}
}
