package spacebirds.widgets;

import javax.sound.sampled.Clip;

import processing.core.*;
import spacebirds.animations.*;
import spacebirds.base.Animation;
import spacebirds.base.Scene;

/**
 * Text Widget handling the click event.
 */
public class ButtonWidget extends EightBitTextWidget {
	
	public interface ClickListener {
		public void buttonClicked(ButtonWidget button);
	};
	
	private int defaultColor, activeColor;
	private ClickListener buttonClickListener = null;
	
	private Animation<Integer> hoverAnimation;
	private Clip buttonClickSound;
	
	private void buttonHovered(boolean hovered) {
		if(hovered) {
			hoverAnimation = new QuadOutAnimation<Integer>( // i don't know what I'm doing
					new LerpColorConverter(this.backgroundColor, activeColor),
					new WidgetBackgroundCallback(this), Animation.VERYFAST);
		} else {
			hoverAnimation = new QuadInAnimation<Integer>(
					new LerpColorConverter(this.backgroundColor, defaultColor),
					new WidgetBackgroundCallback(this), Animation.VERYFAST);
		}
	}
	
	public ButtonWidget(Scene scene, String label) {
		super(scene, label);
		this.setFontSize(20).setPadding(20);
		
		buttonClickSound = p.assetManager().loadClip("button_click.wav");
		
		defaultColor = p.color(80, 80, 160, 128);
		activeColor = p.color(160, 160, 200, 255);
		
		this.setBackground(defaultColor);
		
		if(scene instanceof ClickListener) {
			this.setClickListener((ClickListener) scene);
		}
	}
	
	public ButtonWidget setDefaultColor(int r, int g, int b, int a) {
		defaultColor = p.color(r, g, b, a);
		return this;
	}
	public ButtonWidget setActiveColor(int r, int g, int b, int a) {
		activeColor = p.color(r, g, b, a);
		return this;
	}
	
	@Override
	public void draw() {
		if(hoverAnimation != null) {
			hoverAnimation.update();
			if(hoverAnimation.isDone()) hoverAnimation = null;
		}
		super.draw();
	}
	
	@Override
	public void pause() {
		this.setBackground(defaultColor);
		hoverAnimation = null;
	}
	
	@Override
	public boolean mouseMoved(int pmouseX, int pmouseY, int mouseX, int mouseY) {
		if(this.backgroundColor != activeColor && (hoverAnimation == null || hoverAnimation.isDone())) {
			buttonHovered(true);
		}
		return false;
	}
	
	@Override
	public void mouseEntered(int mouseX, int mouseY) {
		buttonHovered(true);
	}
	@Override
	public void mouseLeft(int mouseX, int mouseY) {
		buttonHovered(false);
	}
	
	@Override
	public boolean mousePressed(int mouseButton, int mouseX, int mouseY) {
		if(mouseButton == PConstants.LEFT && buttonClickListener != null) {
			buttonClickSound.setFramePosition(0);
			buttonClickSound.start();
			buttonClickListener.buttonClicked(this);
			return true;
		}
		return false;
	}
	
	public ButtonWidget setClickListener(ClickListener clickListener) {
		this.buttonClickListener = clickListener;
		return this;
	}

}
