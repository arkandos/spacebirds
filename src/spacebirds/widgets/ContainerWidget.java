package spacebirds.widgets;

import java.util.Collection;

import spacebirds.base.Scene;
import spacebirds.base.Widget;
import spacebirds.base.WidgetManager;

/**
 * Widget containing other widgets.
 * this is similar to your Compounds, but not quite the same.
 * For example, all contained Widgets have to be there at creation time (for simplicity)
 * This widget uses a widgetmanager internally to handle the set of widgets.
 */
public class ContainerWidget extends Widget {

	protected WidgetManager widgetManager;
	private float innerWidth, innerHeight;
	
	private void calculateBoundingBox() {
		x = y = Float.MAX_VALUE;
		float x2 = Float.MIN_VALUE, y2 = Float.MIN_VALUE;
		
		for(Widget widget : widgetManager) {			
			if(widget.getInnerX() < x) {
				x = widget.getInnerX();
			} if(widget.getInnerY() < y) {
				y = widget.getInnerY();
			} if(widget.getInnerX() + widget.getInnerWidth() > x2) {
				x2 = widget.getInnerX() + widget.getInnerWidth();
			} if(widget.getInnerY() + widget.getInnerHeight() > y2) {
				y2 = widget.getInnerY() + widget.getInnerHeight();
			}
			
		}
		innerWidth = x2 - x;
		innerHeight = y2 - y;
	}
	
	public ContainerWidget(Scene scene, Widget... widgets) {
		super(scene);
		widgetManager = new WidgetManager();
		for(Widget widget : widgets) widgetManager.add(widget);
		calculateBoundingBox();
	}
	public ContainerWidget(Scene scene, Collection<Widget> widgets) {
		this(scene, (Widget[]) widgets.toArray());
	}
	
	@Override
	public ContainerWidget setPosition(double x, double y) {
		double dx = x - this.x, dy = y - this.y;
		super.setPosition(x, y);
		for(Widget widget : widgetManager) {
			widget.setPosition(widget.getInnerX() + dx, widget.getInnerY() + dy);
		}
		return this;
	}
	
	public void draw() {
		super.draw();
		widgetManager.drawAll();
	}

	@Override
	public float getInnerWidth() {
		return (float) innerWidth;
	}

	@Override
	public float getInnerHeight() {
		return (float) innerHeight;
	}
	
	@Override
	public Widget collidesImpl(Widget with) {
		for(Widget widget : widgetManager) {
			Widget c = widget.collides(with);
			if(c != null) return c;
		}
		return null;
	}

	@Override
	public boolean keyPressed(char key, int keyCode) {
		return widgetManager.keyPressed(key, keyCode);
	}
	@Override
	public boolean keyReleased(char key, int keyCode) {
		return widgetManager.keyReleased(key, keyCode);
	}
	@Override
	public boolean mousePressed(int mouseButton, int mouseX, int mouseY) {
		return widgetManager.mousePressed(mouseButton, mouseX, mouseY);
	}
	@Override
	public boolean mouseReleased(int mouseButton, int mouseX, int mouseY) {
		return widgetManager.mouseReleased(mouseButton, mouseX, mouseY);
	}
	@Override
	public boolean mouseMoved(int pmouseX, int pmouseY, int mouseX, int mouseY) {
		return widgetManager.mouseMoved(pmouseX, pmouseY, mouseX, mouseY);
	}
	@Override
	public boolean mouseClicked(int mouseButton, int mouseX, int mouseY) {
		return widgetManager.mouseClicked(mouseButton, mouseX, mouseY);
	}
	@Override
	public boolean mouseWheel(int delta, int mouseX, int mouseY) {
		return widgetManager.mouseWheel(delta, mouseX, mouseY);
	}
}
