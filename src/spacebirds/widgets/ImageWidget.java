package spacebirds.widgets;

import processing.core.PImage;
import spacebirds.base.Scene;
import spacebirds.base.Widget;

/**
 * displays an image.
 */
public class ImageWidget extends Widget {

	private PImage image;
	
	public ImageWidget(Scene scene, PImage image) {
		super(scene);
		this.setImage(image);
	}
	
	public ImageWidget(Scene scene, String asset) {
		super(scene);
		this.setImage(asset);
	}
	public ImageWidget(Scene scene, String asset, int width, int height) {
		super(scene);
		this.setImage(asset, width, height);
	}
	
	public ImageWidget setImage(PImage image) {
		this.image = image;
		return this;
	}
	public ImageWidget setImage(String asset) {
		this.image = p.assetManager().loadImage(asset, 0, 0);
		return this;
	}
	public ImageWidget setImage(String asset, int width, int height) {
		this.image = p.assetManager().loadImage(asset, width, height);
		return this;
	}
	
	@Override
	public void draw() {
		super.draw();
		p.image(image, x, y);
	}

	@Override
	public float getInnerWidth() {
		return image.width;
	}

	@Override
	public float getInnerHeight() {
		return image.height;
	}

}
