package spacebirds.widgets;

import spacebirds.base.Scene;
import spacebirds.base.Widget;

/**
 * displays the frameRate variable
 */
public class FpsWidget extends Widget {

	private int precision;
	private String prefix;
	
	private String getFpsString() {
		return prefix + String.valueOf((double)Math.round(p.frameRate * precision) / precision);
	}
	
	public FpsWidget(Scene scene, String prefix, int precision) {
		super(scene);
		this.prefix = prefix;
		this.precision = (int) Math.pow(10, precision);
	}
	
	@Override
	public void draw() {
		super.draw();
		p.fill(255);
		p.textAlign(LEFT, TOP);
		p.text(getFpsString(), x, y);
	}

	@Override
	public float getInnerWidth() {
		return p.textWidth(getFpsString());
	}

	@Override
	public float getInnerHeight() {
		return p.textAscent() + p.textDescent();
	}

}
