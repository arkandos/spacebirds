package spacebirds.animations;

import processing.core.PVector;
import spacebirds.base.Animation;

/**
 * Converter to animate in more dimensions but just one.
 * Useful to animate the position or movement of things,
 * for example using the WidgetPositionCallback.
 * @author User
 *
 */

public class PVectorConverter implements Animation.ValueConverter<PVector> {

	private PVector start;
	private PVector difference;
	
	public PVectorConverter(PVector start, PVector end) {
		this.start = start;
		this.difference = PVector.sub(end, start);
	}
	public PVectorConverter(PVector start, double endx, double endy) {
		this(start, new PVector((float)endx, (float)endy));
	}
	
	@Override
	public PVector convert(double value) {
		return PVector.add(PVector.mult(this.difference, (float) value), start);
	}

}
