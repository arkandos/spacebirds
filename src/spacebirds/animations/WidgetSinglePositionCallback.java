package spacebirds.animations;

import processing.core.PConstants;
import spacebirds.base.Animation;
import spacebirds.base.Widget;

public class WidgetSinglePositionCallback implements Animation.Callback<Double> {

	private int direction;
	private Widget widget;
	
	/**
	 * @param widget widget to animate
	 * @param direction either PConstants.X or PConstants.Y
	 */
	
	public WidgetSinglePositionCallback(Widget widget, int direction) {
		this.widget = widget;
		this.direction = direction;
	}
	
	@Override
	public void start(Animation<Double> animation) {
	}

	@Override
	public void step(Animation<Double> animation, Double value) {
		if(direction == PConstants.X) {
			widget.setPosition(value, widget.getInnerY());
		} else if(direction == PConstants.Y) {
			widget.setPosition(widget.getInnerX(), value);
		}
	}

	@Override
	public void done(Animation<Double> animation, long endOffset) {
	}

}
