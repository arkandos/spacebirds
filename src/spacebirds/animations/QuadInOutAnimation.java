package spacebirds.animations;

import spacebirds.base.Animation;

/**
 * Animation slow at the beginning and the end, but with a speed of 1 at the middle.
 */
public class QuadInOutAnimation<T> extends Animation<T> {

	public QuadInOutAnimation(ValueConverter<T> converter, Callback<T> callback, long duration) {
		super(converter, callback, duration);
	}
	public QuadInOutAnimation(Callback<Double> callback, long duration) {
		super(callback, duration);
	}

	@Override
	public double easing(double linear) {
		return (linear < 0.5) ? (2*linear*linear) : ((-2*linear*linear) + (4*linear) - 1);
	}

}
