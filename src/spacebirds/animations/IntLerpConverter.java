package spacebirds.animations;

import spacebirds.base.Animation;

/**
 * Linear interpolation converter for 2 integers.
 */
public class IntLerpConverter implements Animation.ValueConverter<Integer> {

	private int startValue, endValue;
	public IntLerpConverter(int start, int end) {
		this.startValue = start;
		this.endValue = end;
	}
	@Override
	public Integer convert(double value) {
		return Integer.valueOf((int) Math.floor((endValue-startValue)*value + startValue));
	}

}
