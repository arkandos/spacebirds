package spacebirds.animations;

import processing.core.PVector;
import spacebirds.base.Animation;
import spacebirds.base.Widget;

/**
 * Animate the position of a widget by calling widget.setPosition() with a PVector.
 * This should be used in conjunction with a PVectorConverter.
 * @author User
 *
 */

public class WidgetPositionCallback implements Animation.Callback<PVector> {

	private Widget widget;
	
	public WidgetPositionCallback(Widget widget) {
		this.widget = widget;
	}
	
	@Override
	public void start(Animation<PVector> animation) {
	}

	@Override
	public void step(Animation<PVector> animation, PVector value) {
		widget.setPosition(value);
	}

	@Override
	public void done(Animation<PVector> animation, long endOffset) {
	}

}
