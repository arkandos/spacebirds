package spacebirds.animations;

import processing.core.*;
import spacebirds.base.Animation;

/**
 * converts a double value to an image by imaging to an image array.
 */
public class PImageConverter implements Animation.ValueConverter<PImage> {
	
	private PImage[] images;
	/**
	 * Create the converter directly from an image array.
	 */
	public PImageConverter(PImage... images) {
		this.images = images;
	}
	/**
	 * Splits one image into numberOfSlices parts and uses the slices as the array.
	 * @param image image containing the animation frames one after another.
	 * @param numberOfSlices how many slices the animation has.
	 * @param direction PConstants.X or PConstants.Y to define the direction in which the animation is saved.
	 */
	public PImageConverter(PImage image, int numberOfSlices, int direction) {
		this.images = new PImage[numberOfSlices];
		int sliceWidth = direction == PConstants.X ? (image.width / numberOfSlices) : image.width;
		int sliceHeight = direction == PConstants.Y ? (image.height / numberOfSlices) : image.height;
		
		for(int i = 0; i < numberOfSlices; ++i) {
			int x = (direction == PConstants.X) ? i*sliceWidth : 0;
			int y = (direction == PConstants.Y) ? i*sliceHeight : 0;
			this.images[i] = image.get(x, y, sliceWidth, sliceHeight);
		}
	}
	
	@Override
	public PImage convert(double value) {
		int index = Math.min((int)Math.floor(value * images.length), images.length - 1);
		return images[index];
	}
}
