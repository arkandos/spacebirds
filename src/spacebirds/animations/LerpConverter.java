package spacebirds.animations;

import spacebirds.base.Animation;

/**
 * Simple linear interpolation between a start value and a end value.
 * this is useful if a simple double is all you need or you have to do
 * more complicated stuff which can not be handled by a converter.
 * @author User
 *
 */

public class LerpConverter implements Animation.ValueConverter<Double> {
	private double endValue, startValue;
	public LerpConverter(double startValue, double endValue) {
		this.startValue = startValue;
		this.endValue = endValue;
	}
	@Override
	public Double convert(double value) {
		return (endValue - startValue) * value + startValue;
	}
}
