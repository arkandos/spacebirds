package spacebirds.animations;

import spacebirds.base.Animation;

/**
 * Animation fast at the beginning but continuously slowing down.
 */
public class QuadOutAnimation<T> extends Animation<T> {

	public QuadOutAnimation(ValueConverter<T> converter, Callback<T> callback, long duration) {
		super(converter, callback, duration);
	}
	public QuadOutAnimation(Callback<Double> callback, long duration) {
		super(callback, duration);
	}

	@Override
	public double easing(double linear) {
		return -linear*(linear-2);
	}

}
