package spacebirds.animations;

import processing.core.PApplet;
import processing.core.PConstants;
import spacebirds.base.Animation;

/**
 * A converter which interpolates 2 color values.
 * this can be used for example in conjunction with a WidgetBackgroundCallback.
 *
 */
public class LerpColorConverter implements Animation.ValueConverter<Integer> {
	private int startColor, endColor;
	public LerpColorConverter(int startColor, int endColor) {
		this.startColor = startColor;
		this.endColor = endColor;
	}
	@Override
	public Integer convert(double value) {
		return PApplet.lerpColor(startColor, endColor, (float)value, PConstants.RGB);
	}

}
