package spacebirds.animations;

import spacebirds.base.Animation;
import spacebirds.base.Widget;

/**
 * A callback which calls widget.setBackground() with the (int) color values passed.
 * these color values are the same returned by PApplet.color().
 * You should use this callback in conjunction with a LerpColorConverter.

 */

public class WidgetBackgroundCallback implements Animation.Callback<Integer> {
	
	private Widget widget;
	public WidgetBackgroundCallback(Widget widget) {
		this.widget = widget;
	}
	
	@Override
	public void start(Animation<Integer> animation) {	
	}

	@Override
	public void step(Animation<Integer> animation, Integer value) {
		widget.setBackground(value);
	}

	@Override
	public void done(Animation<Integer> animation, long endOffset) {
	}

}
