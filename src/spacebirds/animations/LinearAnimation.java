package spacebirds.animations;

import spacebirds.base.Animation;

/**
 * Simplest easing Animation just returning the linear values passed by the class.
 */

public class LinearAnimation<T> extends Animation<T> {

	public LinearAnimation(ValueConverter<T> converter, Callback<T> callback, long duration) {
		super(converter, callback, duration);
	}
	public LinearAnimation(Callback<Double> callback, long duration) {
		super(callback, duration);
	}
	public LinearAnimation(Timer timer, long interval) {
		super(timer, interval);
	}

	@Override
	public double easing(double linear) {
		return linear;
	}

}
