package spacebirds.animations;

import processing.core.PImage;
import spacebirds.base.Animation;
import spacebirds.widgets.ImageWidget;

/**
 * Update a ImageWidget with images given by the converter.
 */
public class ImageWidgetCallback implements Animation.Callback<PImage> {

	private ImageWidget widget;
	
	public ImageWidgetCallback(ImageWidget widget) {
		this.widget = widget;
	}
	
	@Override
	public void start(Animation<PImage> animation) {
	}

	@Override
	public void step(Animation<PImage> animation, PImage value) {
		widget.setImage(value);
	}

	@Override
	public void done(Animation<PImage> animation, long endOffset) {	
	}

}
