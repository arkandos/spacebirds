package spacebirds.animations;

import spacebirds.base.Animation;

/**
 * Quadratic in animation. Slow at the beginning, gaining speed while progressing.
 */
public class QuadInAnimation<T> extends Animation<T> {

	public QuadInAnimation(ValueConverter<T> converter, Callback<T> callback, long duration) {
		super(converter, callback, duration);
	}
	public QuadInAnimation(Callback<Double> callback, long duration) {
		super(callback, duration);
	}

	@Override
	public double easing(double linear) {
		return linear*linear;
	}

}
