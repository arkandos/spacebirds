package spacebirds.animations;

import spacebirds.base.Animation;
import spacebirds.widgets.GaugeWidget;

/**
 * Update a gauge widget with the values.
 */
public class GaugeWidgetCallback implements Animation.Callback<Double> {

	protected GaugeWidget gauge;
	public GaugeWidgetCallback(GaugeWidget widget) {
		this.gauge = widget;
	}
	@Override
	public void start(Animation<Double> animation) {		
	}

	@Override
	public void step(Animation<Double> animation, Double value) {
		gauge.setProgress((float)(double)value);
	}

	@Override
	public void done(Animation<Double> animation, long endOffset) {		
	}

}
