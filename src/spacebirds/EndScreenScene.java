package spacebirds;

import spacebirds.base.*;
import spacebirds.scenes.WidgetScene;
import spacebirds.widgets.*;

/**
 * Scene after a game has ended displaying the score reached.
 * Offers options to go to the menu or immediatly start a new game.
 */

public class EndScreenScene extends WidgetScene implements ButtonWidget.ClickListener {
	
	private Widget backToMenuButton, restartButton;
	
	@Override
	public void setup(BaseProgram p, Bundle params) {
		super.setup(p, params);
		
		int points = params.get("points", 0);
		
		this.add(new ImageWidget(this, "background.jpg"));
		
		this.add(new EightBitTextWidget(this, "It's over.")
			.setFontSize(42)
			.alignCenter(0, 0, p.width, p.height / 2));
				
		this.add(new EightBitTextWidget(this, "You've got " + points + " points.")
			.setFontSize(16)
			.alignCenter(0, p.height / 2 - 50, p.width, p.height / 2 - 50));
			
		restartButton = this.add(new ButtonWidget(this, "Restart")
			.paddingToSize(p.width - 100, -1)
			.alignCenter(0, p.height - 70, p.width, p.height - 70));
		
		backToMenuButton = this.add(new ButtonWidget(this, "Back to Menu")
			.paddingToSize(p.width - 100, -1)
			.alignCenter(0, restartButton.getInnerY() - 70, p.width, restartButton.getInnerY() - 70));
	}
	
	/**
	 * Return to the StartMenuScene and immediatly
	 * push a new GameScene on top of it.
	 */
	public void restartGame() {
		p.sm().popScene(StartMenuScene.class);
		p.sm().pushScene(GameScene.class);
	}
	
	public void buttonClicked(ButtonWidget button) {
		if(button == backToMenuButton) {
			p.sm().popScene(StartMenuScene.class);
		} else if (button == restartButton) {
			restartGame();
		}
	}
	
	/**
	 * If you press space, start a new game.
	 * This is for convenience if the player keeps spamming space after loosing :)
	 */
	public void keyPressed(char key, int keyCode) {
		if(key == ' ') {
			restartGame();
		}
	}
}
