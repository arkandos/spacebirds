package spacebirds;

import java.util.Random;

import processing.core.*;
import spacebirds.PlayerObject.PowerUps;
import spacebirds.animations.*;
import spacebirds.base.*;
import spacebirds.widgets.EightBitTextWidget;
import spacebirds.widgets.FpsWidget;
import spacebirds.widgets.GaugeWidget;

/**
 * Main Scene for the Game. This is where all the magic happens.
 * This one doesnt use WidgetManager or AnimationManager because it wants more
 * control over what happens.
 */

public class GameScene extends Scene {
	
	public static final long WINDOW_BORDER_LIFETIME = 500;
	public static final long GAME_SPEED = 4000;
		
	private PImage background_layer1, background_layer2, background_layer3, overlay;
	
	private GaugeWidget windowBorderLife;
	private FpsWidget fpsDisplay;
	private ScoreCounterWidget scoreCounter;
	private PowerupWidget powerupDisplay;
	private GameWorldChunk currentChunk, nextChunk;
	
	private EightBitTextWidget endGameFantasy;
	
	private Animation<Double> pointsTimer, background2Animation, background3Animation, chunksAnimation, windowBorderAnimation;
	
	private PlayerObject player;
	
	@Override
	public void setup(final BaseProgram p, final Bundle params) {
		super.setup(p, params);
		
		//create player; disable gravity at the beginning to help the player figure out what to do.
		player = new PlayerObject(this, 100);
		player.disableGravity();
		player.setPosition(player.getInnerX(), p.height / 2);
		
		powerupDisplay = new PowerupWidget(this);
		powerupDisplay.setPosition(20, 70).setBackground(0, 0, 0, 128).setPadding(10);
		
		//the first chunks.
		currentChunk = new FirstWorldChunk(this, player, new Random());
		nextChunk = currentChunk.next();
		
		//backgrounds consist of 3 layers individually moving
		background_layer1 = p.assetManager().loadImage("background_layer1.jpg", p.width, p.height);
		background_layer2 = p.assetManager().loadImage("background_layer2.png", p.width, p.height);
		background_layer3 = p.assetManager().loadImage("background_layer3.png", p.width, p.height);
		overlay = p.assetManager().loadImage("overlay.png", p.width, p.height);
		
		scoreCounter = new ScoreCounterWidget(this, "Points: ", 0);
		scoreCounter.setFontSize(12).setPosition(20,20).setBackground(0,0,0,128).setPadding(10);
		
		fpsDisplay = new FpsWidget(this, "", 1);
		fpsDisplay.setPosition(p.width - 40, 10);
		
		//this is used to display a timer thingy to visually tell the user how much buffer time he has until he dies
		//if he collides with the edges of the screen.
		windowBorderLife = new GaugeWidget(this);
		windowBorderLife.setPosition(p.width - windowBorderLife.getInnerWidth() - 20, 20).setVisibility(false);
		
		//I have nothing to say about this, how did this even get here?!
		endGameFantasy = new EightBitTextWidget(this, "#EndGameFantasy!");
		endGameFantasy.setFontSize(24)
			.setBackground(0, 0, 0, 128)
			.alignCenter(0, 0, p.width, p.height)
			.paddingToSize(p.width, 80);
		
		//points are just given on a per-time basis. this is a very "lazy" approach, but very simple.
		pointsTimer = new LinearAnimation<Double>(new Animation.Timer() {
			public void onTimer(Animation<?> animation, long offset) {
				if(player.gravityEnabled()) {
					scoreCounter.increment();
				}
			}
			
		}, GAME_SPEED / 3);
		
		//draw second background layer moving.
		background2Animation = new LinearAnimation<Double>(
			new LerpConverter(0, -p.width),
			new Animation.Callback<Double>() {
				@Override public void start(Animation<Double> animation) {
				}
				@Override public void step(Animation<Double> animation, Double value) {
					p.image(background_layer2, (float)(double)value, 0);
					p.image(background_layer2, (float)(double)value + p.width, 0);
				}
				@Override public void done(Animation<Double> animation, long endOffset) {
					animation.reset(endOffset);
				}
		}, GAME_SPEED * 5);
		//draw 3rd background layer moving. this one is moving faster than the second one to
		//create a better parallax space illusion.
		background3Animation = new LinearAnimation<Double>(
				new LerpConverter(0, -p.width),
				new Animation.Callback<Double>() {
					@Override public void start(Animation<Double> animation) {
					}
					@Override public void step(Animation<Double> animation, Double value) {
						p.image(background_layer3, (float)(double)value, 0);
						p.image(background_layer3, (float)(double)value + p.width, 0);
					}
					@Override public void done(Animation<Double> animation, long endOffset) {
						animation.reset(endOffset);
					}
			}, GAME_SPEED * 3);
		
		//moves the game chunks around.
		//if the animation ends, the currentChunk "dies" and is replaced by the next() one.
		chunksAnimation = new LinearAnimation<Double>(
			new LerpConverter(0, -p.width),
			new Animation.Callback<Double>() {
				@Override
				public void start(Animation<Double> animation) {
				}
				@Override
				public void step(Animation<Double> animation, Double value) {
					currentChunk.setPosition(value, 0);
					nextChunk.setPosition(value + currentChunk.getOuterWidth(), 0);
				}
				@Override
				public void done(Animation<Double> animation, long endOffset) {
					currentChunk = nextChunk;
					nextChunk = currentChunk.next();
					nextChunk.setPosition(currentChunk.getOuterWidth(), 0);					
					animation.reset(endOffset);
				}
		}, GAME_SPEED);
	}
	
	@Override
	public void pause() {
		pointsTimer.pause();
		background2Animation.pause();
		background3Animation.pause();
		chunksAnimation.pause();
		currentChunk.pause();
		nextChunk.pause();
		player.pause();
	}
	
	@Override
	public void resume() {
		pointsTimer.resume();
		background2Animation.resume();
		background3Animation.resume();
		chunksAnimation.resume();
		currentChunk.resume();
		nextChunk.resume();
		player.resume();
	} 
	
	public void draw() {
		p.image(background_layer1, 0, 0);
		background2Animation.update();
		background3Animation.update();
		
		pointsTimer.update();
		chunksAnimation.update();
		player.update();
		
		doChunkCollision();
		doWindowCollision();

		nextChunk.draw();
		currentChunk.draw();
		scoreCounter.draw();
		powerupDisplay.draw();
		fpsDisplay.drawIfVisible();
		windowBorderLife.drawIfVisible();
		player.draw();
		
		//this one again -- wtf?
		if(scoreCounter.getScore() == 100) {
			endGameFantasy.draw();
		}
		
		//draw a gradient over the game to make the top and bottom edges smoother.
		p.image(overlay, 0, 0);
	}
	
	/**
	 * Change the Game Speed. This updates the affected animations
	 * for the background and chunks.
	 */
	public void setGameSpeed(long gameSpeed) {
		background2Animation.changeSpeed(gameSpeed * 3);
		background3Animation.changeSpeed(gameSpeed * 5);
		chunksAnimation.changeSpeed(gameSpeed);
	}
	
	/**
	 * Jump a little bit in future.
	 * Don't over-do this or you might break things relying on the done() handlers.
	 */
	public void fastForward(long offset) {
		background2Animation.fastForward(offset);
		background3Animation.fastForward(offset);
		chunksAnimation.fastForward(offset);
	}
	/**
	 * Set the powerup and the display
	 */
	public void setPowerup(PowerUps powerup) {
		player.setPowerup(powerup);
		powerupDisplay.setPowerup(powerup);
	}
	/**
	 * end game if the player collides with a chunk; e.g collides with a hurdle.
	 */
	public void doChunkCollision() {
		if(player.collides(currentChunk) != null || player.collides(nextChunk) != null) {
			endGame();
		}
	}
	/**
	 * If the player collides with the window edges,
	 * an animation is started displaying a gauge widget on the screen.
	 * If this gauge gets completely filled, the game is ended.
	 * This gives the player a little buffer so he does'nt have to fear the window borders.
	 */
	public void doWindowCollision() {
		if(player.getInnerY() < 0 || player.getInnerY() + player.getInnerWidth() > p.height) {
			if(windowBorderAnimation == null) {
				windowBorderAnimation = new LinearAnimation<Double>(
					Animation.PASS_CONVERTER,
					new GaugeWidgetCallback(windowBorderLife) {
						@Override
						public void start(Animation<Double> animation) {
							gauge.setVisibility(true);
						}
						@Override
						public void done(Animation<Double> animation, long endOffset) {
							endGame();
						}
				}, WINDOW_BORDER_LIFETIME);
			}
			windowBorderAnimation.update();
		} else {
			windowBorderAnimation = null;
			windowBorderLife.setVisibility(false);
		}
	}
	/**
	 * end the game by pushing the EndScreenScene.
	 * plays a little sound effect to make it more epic.
	 */
	public void endGame() {
		p.assetManager().loadClip("die.wav").start();
		p.sm().pushScene(EndScreenScene.class, Bundle.create("points", scoreCounter.getScore()));
	}
	
	/**
	 * forward the mousePressed event to the chunks so the blocked hurdles can unblock.
	 */
	@Override
	public void mousePressed(int mouseButton, int mouseX, int mouseY) {
		currentChunk.mousePressed(mouseButton, mouseX, mouseY);
		nextChunk.mousePressed(mouseButton, mouseX, mouseY);
	}
	
	/**
	 * key bindings for the game. space is to jump, F is to use the powerup, ESC pauses the game.
	 */
	
	@Override
	public void keyPressed(char key, int keyCode) {
		switch(key) {
		case ' ':
			if(!player.gravityEnabled()) player.enableGravity();
			player.jump();
			break;
		case 'f':
		case 'F':
			player.powerup();
			powerupDisplay.setPowerup(PowerUps.NONE);
			break;
		case ESC:
			p.sceneManager().pushScene(PauseScene.class, Bundle.create("screenshot", p.get()));
			break;	
		}
	}
}
