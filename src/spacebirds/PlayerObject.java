package spacebirds;

import javax.sound.sampled.Clip;

import processing.core.PImage;
import spacebirds.base.Widget;

/**
 * The Player Object.
 * This has a lot of stuff going on.
 */

public class PlayerObject extends Widget {

	/**
	 * Gravity affecting the player (if active) in pixels per second.
	 */
	public static final double GRAVITY = 2000; // px / s^2
	/**
	 * The Velocity gained by jumping in pixels per second.
	 */
	public static final double JUMP_VELOCITY = 500; // px / s
	
	/**
	 * various powerups to gain effects influencing the gameplay.
	 */
	public static enum PowerUps {
		/**
		 * Key representing no powerup.
		 */
		NONE,
		/**
		 * forward the game a little bit to create the illusion of "beaming" a little bit.
		 */
		FLASH,
		/**
		 * A little fun powerup increasing game speed and jump height.
		 * Also transforms the player to a Koopa and plays a sound from mario.
		 */
		MARIO,
		/**
		 * Makes the player untouchable for a few seconds.
		 * If the player is ghosting, he does'nt collide with anything,
		 * meaning he can pass through hurdles, but can also not pick up powerups.
		 */
		GHOST,
		/**
		 * A very simple powerup which is just a strong jump.
		 */
		CATAPULT
	}
	
	/**
	 * How long the Ghost effect is active.
	 */
	public static final int GHOST_DURATION = (int) GameScene.GAME_SPEED / 2;
	/**
	 * How long you can enjoy being developed by Nintendo.
	 */
	public static final int MARIO_DURATION = (int) GameScene.GAME_SPEED;
	/**
	 * Velocity by catapult jumping.
	 */
	public static final int CATAPULT_VELOCITY = 1000;
	/**
	 * Velocity for the jump during Mario time.
	 */
	public static final int MARIO_VELOCITY = 650;
	/**
	 * How much milliseconds to forward for the flash powerup.
	 */
	public static final long FLASH_FORWARD = GameScene.GAME_SPEED / 5;
	/**
	 * Increased Game Speed using during Ghost and Mario powerups.
	 */
	public static final long INCREASED_GAME_SPEED = (long) (GameScene.GAME_SPEED * 0.8);
	
	public static final int PLAYER_WIDTH = 42;
	public static final int PLAYER_HEIGHT = 22;
	
	private GameScene gameScene;
	
	private boolean gravityEnabled = true;
	private double velocity;
	private PowerUps currentPowerUp = PowerUps.NONE;
	private long lastUpdateTime;
	
	private long marioTime = 0, ghostTime = 0;

	private PImage playerGhost, playerKoopa;
	private PImage playerDown, playerHold, playerUp;
	
	private Clip powerupSetSound, flashSound, ghostSound, marioSound, catapultSound;
	
	public PlayerObject(GameScene scene, double x) {
		super(scene);
		this.gameScene = scene;
		
		this.x = (float) x;
		this.y = 0;
		this.velocity = 0;
		this.lastUpdateTime = System.currentTimeMillis();
		
		this.playerGhost = p.assetManager().loadImage("player_ghost.png", 0, 0);
		this.playerKoopa = p.assetManager().loadImage("player_koopa.png", 0, 0);
		this.playerDown = p.assetManager().loadImage("player_down.png", 0, 0);
		this.playerHold = p.assetManager().loadImage("player_hold.png", 0, 0);
		this.playerUp = p.assetManager().loadImage("player_up.png", 0, 0);
		
		this.powerupSetSound = p.assetManager().loadClip("powerup.wav");
		this.flashSound = p.assetManager().loadClip("flash.wav");
		this.ghostSound = p.assetManager().loadClip("ghost.wav");
		this.marioSound = p.assetManager().loadClip("mario.wav");
		this.catapultSound = p.assetManager().loadClip("jump.wav");

	}
	
	/**
	 * Use the current powerup.
	 * Every powerup plays a sound effect.
	 */
	public void powerup() {
		switch(currentPowerUp) {
		case NONE: return;
		case GHOST:
			ghostSound.setFramePosition(0);
			ghostSound.start();
			gameScene.setGameSpeed(INCREASED_GAME_SPEED);
			ghostTime = GHOST_DURATION; break;
		case MARIO:
			marioSound.setFramePosition(0);
			marioSound.start();
			gameScene.setGameSpeed(INCREASED_GAME_SPEED);
			marioTime = MARIO_DURATION; break;
		case FLASH:
			flashSound.setFramePosition(0);
			flashSound.start();
			gameScene.fastForward(FLASH_FORWARD); break;
		case CATAPULT:
			catapultSound.setFramePosition(0);
			catapultSound.start();
			this.velocity = CATAPULT_VELOCITY; break;
		}
		currentPowerUp = PowerUps.NONE;
	}
	
	/**
	 * Change the current powerup.
	 * this plays a "pick-up" sound.
	 */
	public void setPowerup(PowerUps powerup) {
		currentPowerUp = powerup;
		powerupSetSound.setFramePosition(0);
		powerupSetSound.start();
	}
	public PowerUps getPowerup() {
		return currentPowerUp;
	}
	
	/**
	 * Set the current velocity to the JUMP_VELOCITY or MARIO_VELOCITY
	 */
	public void jump() {
		if(marioTime > 0) this.velocity = MARIO_VELOCITY;
		else this.velocity = JUMP_VELOCITY;
	}
	
	/**
	 * Do exactly that. Bummer.
	 */
	public void enableGravity() {
		gravityEnabled = true;
	}
	/**
	 * ditto
	 */
	public void disableGravity() {
		gravityEnabled = false;
	}
	/**
	 * @return wether the player is affected by gravity
	 */
	public boolean gravityEnabled() {
		return gravityEnabled;
	}
	
	/**
	 * stop all the sounds. pls stahp.
	 */

	@Override
	public void pause() {
		super.pause();
		powerupSetSound.stop();
		flashSound.stop();
		ghostSound.stop();
		marioSound.stop();
		catapultSound.stop();
	}
	
	/**
	 * set the last update time to the current time to ensure no time has passed during pauses.
	 */
	@Override
	public void resume() {
		super.resume();
		lastUpdateTime = System.currentTimeMillis();
	}
	
	/**
	 * do all the physic and update ghost and mario powerups.
	 */
	public void update() {
		long currentTime = System.currentTimeMillis();
		double elapsedSeconds = (currentTime - lastUpdateTime) / 1000.0;
		
		if(ghostTime > 0) ghostTime -= (currentTime - lastUpdateTime);
		if(marioTime > 0) marioTime -= (currentTime - lastUpdateTime);
		
		if((ghostTime != 0 || marioTime != 0) && ghostTime <= 0 && marioTime <= 0) {
			gameScene.setGameSpeed(GameScene.GAME_SPEED);
			ghostTime = marioTime = 0;
		}
		
		//use center to increase the accuracy of this integral,
		//assuming the "infinite" small parts are straight lines
		if(gravityEnabled) {
			this.y += 0.5 * GRAVITY * elapsedSeconds * elapsedSeconds - this.velocity * elapsedSeconds;
			this.velocity -= GRAVITY * elapsedSeconds;
		} else {
			this.y -= this.velocity * elapsedSeconds;
		}

		lastUpdateTime = currentTime;
	}
	
	/**
	 * draw an image according to the current powerup and velocity.
	 */
	@Override
	public void draw() {
		super.draw();
		
		PImage playerImage = null;
		
		if(ghostTime > 0) {
			playerImage = playerGhost;
		} else if(marioTime > 0) {
			playerImage = playerKoopa;
		} else if(Math.abs(this.velocity) < JUMP_VELOCITY / 2) {
			playerImage = playerHold;
		} else if(this.velocity > 0) {
			playerImage = playerUp;
		} else if(this.velocity < 0) {
			playerImage = playerDown;
		}
		
		p.image(playerImage, (float) x, (float) y);
	}

	@Override
	public float getInnerWidth() {
		return PLAYER_WIDTH;
	}

	@Override
	public float getInnerHeight() {
		return PLAYER_HEIGHT;
	}
	
	/**
	 * no collision during ghost.
	 */
	@Override
	protected Widget collidesImpl(Widget with) {
		return ghostTime > 0 ? null : super.collidesImpl(with);
	}
}
