package spacebirds;

import java.util.Random;

import spacebirds.base.Widget;
import spacebirds.widgets.ContainerWidget;

/**
 * A GameWorldChunk is a portion of the GameWorld.
 * This is done to create different level types.
 * Every chunk is as big as the game window.
 * 
 */

public abstract class GameWorldChunk extends ContainerWidget {
	
	protected GameScene scene;
	protected PlayerObject player;
	protected Random rng;
	
	protected GameWorldChunk(GameScene scene, PlayerObject player, Random rng, Widget[] widgets) {
		super(scene, widgets);
		this.scene = scene;
		this.player = player;
		this.rng = rng;
	}
	
	/**
	 * The chunk following this one to create a continueous worlds.
	 */
	public abstract GameWorldChunk next();
	
	public void draw() {
		//p.stroke(255, 0, 0);
		//p.line(x, 0, x, p.height);
		super.draw();
	}

	@Override
	public float getInnerWidth() {
		return p.width;
	}

	@Override
	public float getInnerHeight() {
		return p.height;
	}
}
