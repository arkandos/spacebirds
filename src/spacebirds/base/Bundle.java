package spacebirds.base;

import java.util.HashMap;

/**
 * Wrapper for a Map to simplify paramter passing between Scenes.
 */
public class Bundle {
	private HashMap<String, Object> map;
	
	Bundle() {
		map = new HashMap<String, Object>();
	}
	
	/**
	 * Store values into this Bundle
	 * @return this bundle
	 */
	
	public <T> Bundle put(String key, T object) {
		map.put(key, object);
		return this;
	}
	/** ditto **/
	@SuppressWarnings("unchecked")
	public <T> Bundle put(String key, T... elements) {
		map.put(key, elements);
		return this;
	}
	
	/**
	 * Check if this Bundle holds a value for key.
	 */
	public boolean contains(String key) {
		return map.containsKey(key);
	}
	
	/**
	 * Return the value stored with the name 'key' in this bundle.
	 * If the key does not exist, this throws an ArrayIndexOutOfBoundsException.
	 * @param key index of value.
	 * @return value stored in this bundle
	 * @throws ClassCastException: Type missmatch.
	 * @throws ArrayIndexOutOfBoundsException: key not found in this bundle.
	 */
	
	public final int getInt(String key) {
		return this.get(key);
	}
	/** ditto **/
	public final double getDouble(String key) {
		return this.get(key);
	}
	/** ditto **/
	public final String getString(String key) {
		return this.get(key);
	}
	/** ditto **/
	public final int[] getIntArray(String key) {
		return this.get(key);
	}
	/** ditto **/
	public final double[] getDoubleArray(String key) {
		return this.get(key);
	}
	/** ditto **/
	public final String[] getStringArray(String key) {
		return this.get(key);
	}
	/** ditto **/
	@SuppressWarnings("unchecked")
	public final <T> T[] getArray(String key) {
		return (T[])this.get(key);
	}
	/** ditto **/
	@SuppressWarnings("unchecked")
	public final <T> T[] getArray(String key, Class<T> clazz) {
		return (T[])this.get(key);
	}
	/** ditto **/
	@SuppressWarnings("unchecked")
	public <T> T get(String key) {
		if(map.containsKey(key)) return (T)map.get(key);
		else throw new ArrayIndexOutOfBoundsException("No key '"+key+"' found in this bundle!");
	}
	/** ditto **/
	public <T> T get(Class<T> clazz, String key) {
		if(map.containsKey(key)) return clazz.cast(map.get(key));
		else throw new ArrayIndexOutOfBoundsException("No key '"+key+"' found in this bundle!");
	}
	
	/**
	 * Returns the value associated with a key in this Bundle,
	 * or a default value if no key was found. 
	 * @param key key of the value.
	 * @param defaultValue default value to return if this Bundle doesnt contain the key.
	 * @return value or defaultValue.
	 */
	
	@SuppressWarnings("unchecked")
	public <T> T get(String key, T defaultValue) {
		if(map.containsKey(key)) return(T)map.get(key);
		else return defaultValue;
	}
	/** ditto **/
	public <T> T get(Class<T> clazz, String key, T defaultValue) {
		if(map.containsKey(key)) return clazz.cast(map.get(key));
		else return defaultValue;
	}
	
	/**
	 * Create a new Bundle as a simple function construct.
	 * The key and value pairs are just chained together.
	 * <br>
	 * Example: Bundle.create("arg0", 42, "arg1", 5.3, "arg2", myfancyobject);
	 * 
	 * @param varargs
	 * @return
	 */
	
	public static Bundle create(Object... varargs) {
		Bundle bundle = new Bundle();
		for(int i = 0; i < varargs.length; i += 2) {
			bundle.put((String)varargs[i], varargs[i+1]);
		}
		return bundle;
	}
	@SafeVarargs
	public static <T> T[] array(T... elements) {
		return elements;
	}
	
}
