package spacebirds.base;

import processing.core.PConstants;
import processing.core.PVector;

public abstract class Widget implements PConstants {
	
	protected BaseProgram p;
	protected float x = 0, y = 0;
	protected boolean visible = true;
	protected int backgroundColor;
	protected float paddingLeft = 0, paddingRight = 0, paddingTop = 0, paddingBottom = 0;
	
	public BaseProgram program() { return p; }
	
	public Widget(Scene scene) {
		this.p = scene.program();
		this.backgroundColor = p.color(0, 0);
	}
	
	/**
	 * @return  Left Border of the inner bounding box.
	 */
	
	public final float getInnerX() { return x; }
	/**
	 * @return Top Border of the inner bounding box.
	 */
	public final float getInnerY() { return y; }
	
	public final PVector getInner() { return new PVector(x, y); }
	/**
	 * Set the top left corner of the inner bounding box.
	 */
	public Widget setPosition(double x, double y) {
		this.x = (float) x;
		this.y = (float) y;
		return this;
	}
	public final Widget setPosition(PVector pos) {
		return this.setPosition(pos.x, pos.y);
	}
	
	/**
	 * Set Padding.
	 * The various versions of this function with 1 - 4 arguments behave like the css attribute padding.
	 */
	public final Widget setPadding(double a) {
		return this.setPadding(a, a, a, a);
	}
	/** ditto **/
	 public final Widget setPadding(double a, double b) {
		 return this.setPadding(a, b, a, b);
	 }
	 /** ditto **/
	 public final Widget setPadding(double a, double b, double c) {
		 return this.setPadding(a, b, c, b);
	 }
	 /** ditto **/
	 public Widget setPadding(double a, double b, double c, double d) {
		 if(a >= 0) paddingTop = (float) a;
		 if(b >= 0) paddingRight = (float) b;
		 if(c >= 0) paddingBottom = (float) c;
		 if(d >= 0) paddingLeft = (float) d;
		 return this;
	 }
	 
	 /** set the visibility of the widget. this has no direct effect on wether a widget is drawed or not,
	  * but should rather be handled by the draw() functions or by the scenes or managers.
	  */
	 public Widget setVisibility(boolean visible) {
		 this.visible = visible;
		 return this;
	 }
	 public boolean isVisible() {
		 return visible;
	 }
	 
	 public final Widget paddingToSize(double w, double h) {
		 return this.setPadding((h - this.getInnerHeight()) / 2.0, (w - this.getInnerWidth()) / 2.0);
	 }
	 
	 /**
	  * Set Background Color.
	  * This is usefull in conjunction with padding to create borders around widgets.
	  */
	 public Widget setBackground(double r, double g, double b, double a) {
		 this.backgroundColor = p.color((float) r, (float) g, (float) b, (float) a);
		 return this;
	 }
	 /** ditto **/
	 public Widget setBackground(int color) {
		 this.backgroundColor = color;
		 return this;
	 }
	 /** ditto **/
	 public Widget noBackground() {
		 this.backgroundColor = p.color(0, 0);
		 return this;
	 }
	 
	 public final int getBackgroundColor() {
		 return this.backgroundColor;
	 }
	
	/**
	 * @return Left Border of the outer bounding box (including padding)
	 */
	public final float getOuterX() { return x - paddingLeft; }
	/**
	 * @return Top Border of the outer bounding box (including padding)
	 */
	public final float getOuterY() { return y - paddingTop; }
	
	public final PVector getOuter() {
		return new PVector(x - paddingLeft, y - paddingTop);
	}
	
	/**
	 * Set the position of this Widget on a way that its center is centered in the specified box
	 * given by the parameters.
	 */
	public final Widget alignCenter(double x0, double y0, double x1, double y1) {
		double posx = x0 + (x1 - x0) / 2.0 - this.getInnerWidth() / 2.0;
		double posy = y0 + (y1 - y0) / 2.0 - this.getInnerHeight() / 2.0;
		
		return this.setPosition(posx, posy);
	}
	/**
	 * @return Width of the (outer) bounding box;
	 * that is with the padding.
	 */
	public final float getOuterWidth() {
		return this.getInnerWidth() + paddingLeft + paddingRight;
	}
	/**
	 * @return Height of the (outer) bounding box;
	 * that is with the padding.
	 */
	public final float getOuterHeight() {
		return this.getInnerHeight() + paddingTop + paddingBottom;
	}
	
	public abstract float getInnerWidth();
	public abstract float getInnerHeight();
	
	/**
	 * @return true if the coordinates given are inside the outer bounding box of this widget.
	 */
	public boolean containsCoordinates(double x, double y) {
		return (x >= getOuterX() && x <= getOuterX() + getOuterWidth()
				&& y >= getOuterY() && y <= getOuterY() + getOuterHeight());
	}
	
	public final boolean innerBoundsCollide(Widget with) {
		return this.x < with.x + with.getInnerWidth() &&
				with.x < this.x + this.getInnerWidth() &&
				this.y < with.y + with.getInnerHeight() &&
				with.y < this.y + this.getInnerHeight();
	}
	
	protected Widget collidesImpl(Widget with) {
		if(innerBoundsCollide(with))
		{
			return with;
		} else return null;
	}
	
	public final Widget collides(Widget with) {
		 Widget c1 = this.collidesImpl(with);
		 Widget c2 = with.collidesImpl(this);
		 if(c1 != null && c2 != null) return c1;
		 else return null;
	}
	
	public void draw() {
		if(backgroundColor != p.color(0, 0) && visible) {
			p.noStroke();
			p.fill(backgroundColor);
			p.rect(this.getOuterX(), this.getOuterY(), this.getOuterWidth(), this.getOuterHeight());
		}
	}
	
	public final void drawIfVisible() {
		if(isVisible()) draw();
	}
	
	
	//// EVENT HANDLING
	
	public void pause() {
	}
	public void resume() {
	}
	
	public boolean keyPressed(char key, int keyCode) {
		return false;
	}

	public boolean keyReleased(char key, int keyCode) {
		return false;
	}

	public boolean mousePressed(int mouseButton, int mouseX, int mouseY) {
		return false;
	}

	public boolean mouseReleased(int mouseButton, int mouseX, int mouseY) {
		return false;
	}

	public boolean mouseMoved(int pmouseX, int pmouseY, int mouseX, int mouseY) {
		return false;
	}

	public boolean mouseClicked(int mouseButton, int mouseX, int mouseY) {
		return false;
	}

	public boolean mouseWheel(int delta, int mouseX, int mouseY) {
		return false;
	}
	
	public void mouseEntered(int mouseX, int mouseY) {
	}
	public void mouseLeft(int mouseX, int mouseY) {
	}
	
	public void focusDropped() {
	}
	public void focusGained() {
	}	
	
}
