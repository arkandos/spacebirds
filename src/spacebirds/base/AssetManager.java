package spacebirds.base;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.sound.sampled.*;

import processing.core.*;

/**
 * Load Assets of the game and store them in the AssetManager.
 * This allows for preloading, sharing of assets over scenes,
 * and reduces loading times.
 */

public final class AssetManager {
	
	private Map<String, PImage> loadedImages;
	private Map<String, Clip> loadedClips;
	
	private PApplet p;
	
	public AssetManager(PApplet parent) {
		loadedImages = new HashMap<String, PImage>();
		loadedClips = new HashMap<String, Clip>();
		this.p = parent;	
	}
	
	public void clear() {
		loadedImages.clear();
	}
	
	/**
	 * Create a hash key for an image of a size.
	 */
	private String createImageKey(String assetName, int width, int height) {
		return "Image_" + assetName + "_" + width + "_" + height;
	}
	
	/**
	 * returns whether an image is already loaded or not.
	 */
	public boolean isImageLoaded(String assetName, int width, int height) {
		return loadedImages.containsKey(createImageKey(assetName, width, height));
	}
	
	/**
	 * returns an already loaded image or null.
	 */
	public PImage getImage(String assetName, int width, int height) {
		String resourceKey = createImageKey(assetName, width, height);
		if(loadedImages.containsKey(resourceKey)) {
			return loadedImages.get(resourceKey);
		} else return null;
	}
	
	/**
	 * ensures an image is loaded (that is, if the image is loaded this function does the same as getImage)
	 * resizes it and returns it.
	 */
	public PImage loadImage(String assetName, int width, int height) {
		if(!isImageLoaded(assetName, width, height)) {
			PImage image = p.loadImage("images/" + assetName);
			if(image != null) {
				if(width != 0 || height != 0) image.resize(width, height);
				loadedImages.put(createImageKey(assetName, image.width, image.height), image);
				loadedImages.put(createImageKey(assetName, width, height), image);
				loadedImages.put(createImageKey(assetName, 0, 0), image);
			}
		}
		
		return getImage(assetName, width, height);

	}
	/**
	 * Check if a audio clip is loaded.
	 */
	public boolean isClipLoaded(String assetName) {
		return loadedClips.containsKey(assetName);
	}
	/**
	 * Return an already loaded audio clip or null.
	 */
	public Clip getClip(String assetName) {
		if(loadedClips.containsKey(assetName)) {
			return loadedClips.get(assetName);
		} else return null;
	}
	/**
	 * Try to load the audio clip if it is'nt loaded yet,
	 * returning the clip. if the clip couldn't load return null.
	 */
	public Clip loadClip(String assetName) {
		if(!loadedClips.containsKey(assetName)) {
			try {
				Clip clip = AudioSystem.getClip();
				AudioInputStream as = AudioSystem.getAudioInputStream(
						new BufferedInputStream(
								ClassLoader.getSystemResourceAsStream("sounds/"  + assetName)));
				clip.open(as);
				loadedClips.put(assetName, clip);
			} catch (LineUnavailableException | UnsupportedAudioFileException | IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		Clip clip = loadedClips.get(assetName);
		return clip;
	}
}
