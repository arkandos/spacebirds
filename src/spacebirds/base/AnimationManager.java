package spacebirds.base;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * This class can be used to update many animations at once.
 * animation objects which are "done" are automatically removed
 * to minimize overhead
 */

public class AnimationManager {
	private LinkedList<Animation<?>> animations;
	
	public AnimationManager() {
		this.animations = new LinkedList<Animation<?>>();
	}
	
	public Animation<?> add(Animation<?> animation) {
		this.animations.add(animation);
		return animation;
	}
	
	public void clear() {
		this.animations.clear();
	}
	
	public void update() {
		for(Iterator<Animation<?>> it = animations.iterator(); it.hasNext();) {
			Animation<?> animation = it.next();
			
			animation.update();
			
			if(animation.isDone()) {
				it.remove();
			}
		}
	}
	
	public void remove(Animation<?> animation) {
		this.animations.remove(animation);
	}
	public void stopAll() {
		this.animations.clear();
	}
	public void pauseAll() {
		for(Animation<?> animation : animations) {
			animation.pause();
		}
	}
	public void resumeAll() {
		for(Animation<?> animation : animations) {
			animation.resume();
		}
	}
	public void resetAll(long offset) {
		for(Animation<?> animation : animations) {
			animation.reset(offset);
		}
	}
}
