package spacebirds.base;

import processing.core.PConstants;

/**
 * A Scene is like a processing applet, but can be changed at runtime
 * and replaced by another.
 */
public class Scene implements PConstants {
	protected BaseProgram p;
	
	public Scene() {
		this.p = null;
	}
	
	/**
	 * Return the BaseProgram object this Scene is currently associated with.
	 */
	public BaseProgram program() { return p; }
	/**
	 * Called the Scene get pushed into the foreground for the first time.
	 */
	public void setup(BaseProgram parent, Bundle params) {
		this.p = parent;
	}
	/**
	 * Called once the Scene is popped from the scene stack.
	 */
	public void stop() {
	}
	public void draw() {
		p.textAlign(PConstants.CENTER, PConstants.CENTER);
		p.text("Empty Scene", 0, 0, p.width, p.height);
	}

	/**
	 * Called once this Scene gets pushed into the background and another scene becomes active.
	 */
	public void pause() {
	}
	/**
	 * Called once the Scene overlapping this one gets popped and this Scene becomes active again.
	 */
	public void resume() {
	}
	public void keyPressed(char key, int keyCode) {
	}
	public void keyReleased(char key, int keyCode) {
	}
	public void mousePressed(int mouseButton, int mouseX, int mouseY) {		
	}
	public void mouseReleased(int mouseButton, int mouseX, int mouseY) {		
	}
	public void mouseMoved(int pmouseX, int pmouseY, int mouseX, int mouseY) {		
	}
	public void mouseDragged(int mouseButton, int pmouseX, int pmouseY, int mouseX, int mouseY) {		
	}
	public void mouseClicked(int mouseButton, int mouseX, int mouseY) {		
	}
	public void mouseWheel(int delta, int mouseX, int mouseY) {		
	}
}
