package spacebirds.base;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Handles a set of widgets.
 * This usually forwards all events to the widgets, stopping at the first one returning true
 * if the event handler returns a boolean value.
 * mouse events are only forwarded if the event is generated on the widget.
 * All events are handled from the visual front to back.
 * @author User
 *
 */
public class WidgetManager implements Iterable<Widget> {
	private Widget currentFocusedWidget = null;
	private ArrayList<Widget> widgets;
	
	public WidgetManager() {
		widgets = new ArrayList<Widget>();
	}
	
	public Widget add(Widget widget) {
		this.widgets.add(widget);
		return widget;
	}
	public boolean remove(Widget widget) {
		return this.widgets.remove(widget);
	}
	
	public boolean contains(Widget widget) {
		return this.widgets.contains(widget);
	}
	
	public int size() {
		return widgets.size();
	}
	
	public Widget get(int i) {
		return widgets.get(i);
	}
	
	public void clear() {
		widgets.clear();
	}
	
	public Iterator<Widget> iterator() {
		return widgets.iterator();
	}
	
	public void setFocus(Widget focusedWidget) {
		if(currentFocusedWidget != null) {
			currentFocusedWidget.focusDropped();
		}
		currentFocusedWidget = focusedWidget;
		if(currentFocusedWidget != null) {
			currentFocusedWidget.focusGained();
		}
	}
	
	public Widget getFocusedWidget() {
		return currentFocusedWidget;
	}
	
	public void pauseAll() {
		for(Widget widget : widgets) {
			widget.pause();
		}
	}

	public void resumeAll() {
		for(Widget widget : widgets) {
			widget.resume();
		}
	}
	
	public void drawAll() {
		for(Widget widget : widgets) if(widget.isVisible()) {
			widget.draw();
		}
	}
	
	public boolean keyPressed(char key, int keyCode) {
		//the focused widget gets priority for all key events.
		if(currentFocusedWidget != null && currentFocusedWidget.keyPressed(key, keyCode)) {
			return true;
		}
			
		for(int i = widgets.size() - 1; i >= 0; --i) {
			Widget widget = widgets.get(i);
			boolean handledSuccessful = widget.keyPressed(key, keyCode);
			if(handledSuccessful) return true;
		}
		return false;
	}

	public boolean keyReleased(char key, int keyCode) {
		if(currentFocusedWidget != null && currentFocusedWidget.keyReleased(key, keyCode)) {
			return true;
		}
			
		for(int i = widgets.size() - 1; i >= 0; --i) {
			Widget widget = widgets.get(i);
			boolean handledSuccessful = widget.keyReleased(key, keyCode);
			if(handledSuccessful) return true;
		}	
		return false;
	}

	public boolean mousePressed(int mouseButton, int mouseX, int mouseY) {
		//since we draw from the first widget to the last in the array,
		//we can handle the events in the right order by reversing the
		//array, asking the top-most widget to handle a event first.
		for(int i = widgets.size() - 1; i >= 0; --i) {
			Widget widget = widgets.get(i);
			if(widget.isVisible() && widget.containsCoordinates(mouseX, mouseY)) {
				boolean handledSuccessful = widget.mousePressed(mouseButton, mouseX, mouseY);
				if(handledSuccessful) return true;
			}
		}
		return false;
	}

	public boolean mouseReleased(int mouseButton, int mouseX, int mouseY) {
		for(int i = widgets.size() - 1; i >= 0; --i) {
			Widget widget = widgets.get(i);
			if(widget.isVisible() && widget.containsCoordinates(mouseX, mouseY)) {
				boolean handledSuccessful = widget.mouseReleased(mouseButton, mouseX, mouseY);
				if(handledSuccessful) return true;
			}
		}
		return false;
	}

	private boolean mouseMovedDraggedHandler(int pmouseX, int pmouseY, int mouseX, int mouseY) {
		boolean handledSuccessful = false;

		for(int i = widgets.size() - 1; i >= 0; --i) {
			Widget widget = widgets.get(i);
			if(widget.isVisible() && widget.containsCoordinates(mouseX, mouseY)) {

				if(!widget.containsCoordinates(pmouseX, pmouseY)) {
					widget.mouseEntered(mouseX, mouseY);
				}
				if(!handledSuccessful) {
					widget.mouseMoved(pmouseX, pmouseY, mouseX, mouseY);
					handledSuccessful = true;
				}
			} else if(widget.containsCoordinates(pmouseX, pmouseY)) {
				widget.mouseLeft(mouseX, mouseY);
			}
		}
		return handledSuccessful;
	}
	
	public boolean mouseMoved(int pmouseX, int pmouseY, int mouseX, int mouseY) {
		return mouseMovedDraggedHandler(pmouseX, pmouseY, mouseX, mouseY);
	}

	public boolean mouseDragged(int mouseButton, int pmouseX, int pmouseY, int mouseX, int mouseY) {
		return mouseMovedDraggedHandler(pmouseX, pmouseY, mouseX, mouseY);
	}

	public boolean mouseClicked(int mouseButton, int mouseX, int mouseY) {
		boolean changedFocus = false, handledSuccessful = false;
		
		for(int i = widgets.size() - 1; i >= 0; --i) {
			Widget widget = widgets.get(i);
			if(widget.isVisible() && widget.containsCoordinates(mouseX, mouseY)) {
				//bonus: change focus!
				if(changedFocus == false) {
					changedFocus = true;
					this.setFocus(widget);
				}

				handledSuccessful = widget.mouseClicked(mouseButton, mouseX, mouseY);
				if(handledSuccessful) break;
			}
		}
		
		if(changedFocus == false) this.setFocus(null);
		return handledSuccessful;
	}

	public boolean mouseWheel(int delta, int mouseX, int mouseY) {
		for(int i = widgets.size() - 1; i >= 0; --i) {
			Widget widget = widgets.get(i);
			if(widget.isVisible() && widget.containsCoordinates(mouseX, mouseY)) {
				boolean handledSuccessful = widget.mouseWheel(delta, mouseX, mouseY);
				if(handledSuccessful) return true;
			}
		}
		return false;
	}
	
}
