package spacebirds.base;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * A SceneManager organises Scenes on a stack-like base.
 * Scenes are only constructed once, to reduce switch times.
 */
public class SceneManager {
	
	private BaseProgram parent;
	private HashMap<Class<? extends Scene>, Scene> constructedScenes;
	private Deque<Scene> sceneStack;
	
	public SceneManager(BaseProgram baseProgram) {
		parent = baseProgram;
		constructedScenes = new HashMap<Class<? extends Scene>, Scene>();
		sceneStack = new LinkedList<Scene>();
		pushScene(Scene.class);
	}
	
	/**
	 * Push a new Scene into the foreground.
	 * This creates the Scene if it isn't constructed yet and calls setup and resume on her.
	 * The previous scene gets moved into the background and paused().
	 */
	public boolean pushScene(Class<? extends Scene> sceneClass) {
		return this.pushScene(sceneClass, new Bundle());
	}
	
	public boolean pushScene(Class<? extends Scene> sceneClass, Bundle params) {
		Scene next;
		if(constructedScenes.containsKey(sceneClass)) {
			next = constructedScenes.get(sceneClass);
		} else {
			try {
				next = sceneClass.newInstance();
				constructedScenes.put(sceneClass, next);
			} catch (InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		
		
		Scene current = current();
		if(current != null) current.pause();
		
		next.setup(parent, params);
		next.resume();
		
		sceneStack.addFirst(next);
		
		return true;
	}
	/**
	 * Pauses() and stops() the current scene, removing it from the stack.
	 * resumes() the scene under it, bringing it to he front.
	 * @return
	 */
	public boolean popScene() {
		if(sceneStack.size() <= 1) return false;
		
		Scene current = sceneStack.pollFirst();
		current.pause();
		current.stop();
		
		Scene next = sceneStack.peekFirst();
		next.resume();
		
		return true;
	}
	
	/**
	 * pop scenes until a specific scene is reached.
	 */
	public boolean popScene(Class<? extends Scene> sceneClass) {
		if(sceneStack.size() <= 1) return false;

		//stop current scene
		Scene current = sceneStack.pollFirst();
		current.pause();
		current.stop();
		//advance scene stack until sceneClass == currentClass
		while(sceneStack.size() > 1 && !sceneStack.peekFirst().getClass().equals(sceneClass)) {
			sceneStack.pollFirst().stop();
		}
		
		Scene next = sceneStack.peekFirst();
		next.resume();
		
		return next.getClass().equals(sceneClass);
	}
	/**
	 * returns the current active scene.
	 */
	public Scene current() {
		return sceneStack.peekFirst();
	}
}
