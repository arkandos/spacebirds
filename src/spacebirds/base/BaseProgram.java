package spacebirds.base;


import java.util.prefs.Preferences;

import processing.core.*;
import processing.event.MouseEvent;
import spacebirds.base.AssetManager;
import spacebirds.base.SceneManager;

/**
 * A applet class for apps using the spacebirds framework.
 * this initializes managers and forwards events.
 */
public class BaseProgram extends PApplet {
	private static final long serialVersionUID = 1L;
	
	protected Preferences preferences;
	protected AssetManager assetManager;
	protected SceneManager sceneManager;
	
	public BaseProgram() {
		preferences = Preferences.userNodeForPackage(this.getClass());
		sceneManager = new SceneManager(this);
		assetManager = new AssetManager(this);
	}
	
	public AssetManager assetManager() { return assetManager; }
	public AssetManager am() { return assetManager; }
	public SceneManager sceneManager() { return sceneManager; }
	public SceneManager sm() { return sceneManager; }
	public Preferences preferences() { return preferences; }
    public Preferences prefs() { return preferences; }

	@Override public void draw() {
		sm().current().draw();
	}
	@Override public void keyPressed() {
		sm().current().keyPressed(key, keyCode);
	}
	@Override public void keyReleased() {
		sm().current().keyReleased(key, keyCode);
	}
	@Override public void mousePressed() {
		sm().current().mousePressed(mouseButton, mouseX, mouseY);
	}
	@Override public void mouseReleased() {
		sm().current().mouseReleased(mouseButton, mouseX, mouseY);
	}
	@Override public void mouseMoved() {
		sm().current().mouseMoved(pmouseX, pmouseY, mouseX, mouseY);
	}
	@Override public void mouseDragged() {
		sm().current().mouseDragged(mouseButton, pmouseX, pmouseY, mouseX, mouseY);
	}
	@Override public void mouseClicked() {
		sm().current().mouseClicked(mouseButton, mouseX, mouseY);
	}
	@Override public void mouseWheel(MouseEvent event) {
		sm().current().mouseWheel(event.getCount(), mouseX, mouseY);
	}
	
	public static final void init(Class<? extends BaseProgram> clazz) {
		PApplet.main(clazz.getCanonicalName());
	}
}
