package spacebirds.base;

/**
 * Animations consist of 3 parts:
 *  * The easing function which defines how the animation "looks"
 *  * The callback which does something with the values, like assign them to a variable or call a setter method
 *  * the converter, which brings the double value from the easing function to the right type for the callback.
 *    (this step is basically obsolete, but simplifies things greatly if you want to reuse your code)
 *    
 * The whole functions looks like
 * Double => Double (easing) ° Double => T (convert) ° T => void ( usage )
 *    
 * All classes derived from this Animation class define a easing function by implementing the abstract method.
 * So there could be subclasses like 'LinearAnimation' 'QuadInAnimatino' or 'SineInOutAnimation'.
 * 
 * Actually using the animation is up to the User or maybe an 'AnimationManager' class in the future.
 * The user of an animation has to just call the update() method repeatedly.
 * Once a animation isDone() these calls will not have any effect. 
 */

public abstract class Animation<T> {
	
	public static final long VERYFAST = 80;
	public static final long FAST = 200;
	public static final long DEFAULT = 400;
	public static final long SLOW = 600;
	public static final long VERYSLOW = 2000;
	
	/**
	 * implementations of this interface convert the eased value to a value the callback can use.
	 */
	public static interface ValueConverter<T> {
		public T convert(double value);
	}
	
	public static final ValueConverter<Double> PASS_CONVERTER = new ValueConverter<Double>() {
		public Double convert(double value) { return value; }
	};
	
	/**
	 * handles animation events.
	 * * start() gets called on the first call to Animation.update()
	 * * step(T value) gets called every time update() gets called afterwards whith the value converted from the Converter
	 * * done() gets called once the animation is finished. It is ensured that step() is called at least once before with the end value.
	 */
	public static interface Callback<T> {
		public void start(Animation<T> animation);
		public void step(Animation<T> animation, T value);
		public void done(Animation<T> animation, long endOffset);
	}
	
	public static interface Timer {
		public void onTimer(Animation<?> animation, long offset);
	}
	
	private ValueConverter<T> converter;
	private Callback<T> callback;
	//startTime is a magic variable.
	//>  0 => animation currently running
	//== 0 => animation not yet started
	// < 0 => animation finished.
	private long startTime, pauseTime, duration;
	private boolean reverse = false;
	
	public Animation(ValueConverter<T> converter, Callback<T> callback, long duration) {
		this.converter = converter;
		this.callback = callback;
		this.duration = duration;
		this.startTime = 0;
		this.pauseTime = -1; // no pause
	}
	
	@SuppressWarnings("unchecked")
	public Animation(Callback<Double> callback, long duration) {
		this((ValueConverter<T>) PASS_CONVERTER, (Callback<T>) callback, duration);
	}
	@SuppressWarnings("unchecked")
	public Animation(final Timer timer, long interval) {
		this((ValueConverter<T>) PASS_CONVERTER, new Callback<T>() {
			@Override
			public void start(Animation<T> animation) {
			}
			@Override
			public void step(Animation<T> animation, T value) {
			}
			@Override
			public void done(Animation<T> animation, long endOffset) {
				animation.reset(endOffset);
				timer.onTimer(animation, endOffset);
			}
		}, interval);
	}
	
	/**
	 * @return true after callback.done() got called.
	 */
	public boolean isDone() {
		return startTime < 0;
	}
	/**
	 * @return true after callback.start() got called. Note this returns true even after the animation isDone()
	 */
	public boolean isStarted() {
		return startTime != 0;
	}
	
	/**
	 * @return true after this animation got paused.
	 */
	public boolean isPaused() {
		return pauseTime > -1;
	}
	
	/**
	 * pause this animation.
	 * a paused animation will do nothing during update() calls and will not advance until resume() gets called.
	 * a animation which not yet started (at least one call to update()) cannot be paused.
	 */
	public void pause() {
		//store the already elapsed time for this animation in pauseTime
		if(!isPaused() && isStarted()) {
			pauseTime = System.currentTimeMillis() - startTime;
		}
	}
	
	public void resume() {
		//change startTime so that the animation appears to be started such that the
		//elapsed time is pauseTime.
		if(isPaused()) {
			startTime = System.currentTimeMillis() - pauseTime;
			pauseTime = -1;
		}
	}
	
	/**
	 * Resets the animation timer.
	 * This causes isDone() and isStarted() to be false and
	 * causes and immediate update().
	 * @param offset how long before it "started"
	 */
	public void reset(long offset) {
		startTime = System.currentTimeMillis() - offset;
		this.callback.start(this);
	}
	
	/**
	 * "Hard-turn" for animations. this lets the animation run backwards starting from their
	 * current point.
	 * Already finished Animatinos get restarted and run completly backwards.
	 * This is usefull to create bouncing effects using InOut Easing functions.
	 * This causes an immediate update().
	 * @param offset how long before it "started"
	 */
	public void reverse(long offset) {
		long currentTime = System.currentTimeMillis();
		if(isDone()) {
			startTime = currentTime - offset;
			reverse = !reverse;
			this.callback.start(this);
		} else {
			//startTime = currentTime - (duration - (currentTime - startTime)) - offset
			this.startTime = 2*currentTime - duration - startTime - offset;
			reverse = !reverse;
			update();
		}
	}
	
	/**
	 * Change the Duration of the Animation.
	 * If this animation is currently running, it proceeds using the new speed.
	 * @param newDuration
	 */
	public void changeSpeed(long newDuration) {
		if(startTime > 0) {
			long currentTime = System.currentTimeMillis();
			//TODO what happens if paused
			double linear = (double)(currentTime-startTime) / duration;
			this.startTime = (long) (currentTime - linear * newDuration);
		}
		this.duration = newDuration;
	}
	/**
	 * forwards the animation by the given offset,
	 * this is like jumping in time.
	 */
	public void fastForward(long offset) {
		if(pauseTime > 0) {
			pauseTime += offset;
		} else if(startTime > 0) {
			startTime -= offset;
		}
	}
	/**
	 * Return the duration.
	 */
	public long getDuration() {
		return duration;
	}
	/**
	 * Return the time this animation already run.
	 */
	public long getElapsed() {
		if(pauseTime > 0) return pauseTime;
		if(startTime > 0) return System.currentTimeMillis() - startTime;
		return -1;
	}
	
	/**
	 * Subclasses should implement this function to create various easing functions.
	 * @param linear a value between [0;1] which grows in a linear manner over the duration of the animation
	 * @return a value between [0;1]
	 */
	public abstract double easing(double linear);
	
	/**
	 * To use an animation, call this function repeatedly.
	 * There is a little bit of magic involved so that you don't have to do anything else,
	 * like manually starting the animation.
	 */
	public void update() {
		
		long currentTime = System.currentTimeMillis();
		
		if(startTime == 0) { // animation not yet started
			startTime = currentTime;
			this.callback.start(this);
		} else if(startTime < 0 || pauseTime >= 0) { //animation done or paused.
			return;
		} else if(currentTime < startTime + duration) { // animation running

			double linear = (double)(currentTime-startTime) / duration;
			if(reverse) linear = 1 - linear;
			T converted = this.converter.convert(this.easing(linear));

			this.callback.step(this, converted);
		} else { // animation finished, but not yet called done()
			//negative min is the time we "overshot" the done time.
			this.startTime = -Math.max((currentTime-startTime) - duration, 1);
			this.callback.step(this, this.converter.convert(this.reverse ? 0.0 : 1.0));
			this.callback.done(this, -this.startTime);
		}
		
	}
	
}
