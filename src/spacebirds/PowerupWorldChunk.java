package spacebirds;

import java.util.Random;

import processing.core.PVector;
import spacebirds.PlayerObject.PowerUps;

/**
 * BasicWorldChunk additionally creating a random powerup.
 */
public class PowerupWorldChunk extends BasicWorldChunk {

	public static PowerUps[] POWERUPS = new PowerUps[] { PowerUps.FLASH, PowerUps.GHOST, PowerUps.MARIO, PowerUps.CATAPULT };
	
	private PowerupWidget powerup;
	private PVector powerupPosition;
	
	public PowerupWorldChunk(GameScene scene, PlayerObject player, Random rng) {
		super(scene, player, rng);
		powerup = new PowerupWidget(scene);
		powerup.setPowerup(POWERUPS[ rng.nextInt(POWERUPS.length) ]);
		powerup.setBackground(0, 0, 0, 128).setPadding(5);
		
		powerup.setPosition(64 + rng.nextInt(p.width - 128), 64 + rng.nextInt(p.height - 128));
		//move the powerup arround if it collides with a hurdle, so you always can collect it.
		while(powerup.collides(this) != null) {
			powerup.setPosition(64 + rng.nextInt(p.width - 128), 64 + rng.nextInt(p.height - 128));
		}
		powerupPosition = powerup.getInner();
	}
	
	/**
	 * set the powerup and hide the display.
	 */
	private void givePowerup() {
		scene.setPowerup(powerup.getPowerup());
		powerup.setVisibility(false);
	}
	
	
	@Override
	public void draw() {
		super.draw();
		
		powerup.setPosition(powerupPosition.x + x, powerupPosition.y + y);
		powerup.drawIfVisible();
		
		//check if the user collides with the powerup and collect it
		if(powerup.isVisible() && player.collides(powerup) != null) {
			givePowerup();
		}
	}
	
	/**
	 * after a powerup world chunk follows a basic world chunk always.
	 * no powerup spammerino!
	 */
	
	@Override
	public GameWorldChunk next() {
		return new BasicWorldChunk(scene, player, rng);
	}
	
	/**
	 * check if the user clicked on the powerup and collect it.
	 */
	@Override
	public boolean mousePressed(int mouseButton, int mouseX, int mouseY) {
		super.mousePressed(mouseButton, mouseX, mouseY);
		if(powerup.containsCoordinates(mouseX, mouseY)) {
			givePowerup();
			return true;
		}
		return false;
	}
}
