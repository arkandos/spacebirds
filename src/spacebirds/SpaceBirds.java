package spacebirds;

import java.awt.event.KeyEvent;

import javax.sound.sampled.Clip;

import processing.core.*;
import spacebirds.base.BaseProgram;

/**
 * Main Application class.
 * This load all global stuff like the cursor and the ambient sound
 * and initializes the window title and the managers.
 * Starts the SpaceBirdsLoadingScene starting the StartMenuScene.
 */
public class SpaceBirds extends BaseProgram {
	private static final long serialVersionUID = 1L;
	
	private PImage cursor;
	private Clip ambientSound;
	
	@Override
	public void setup() {
		size(640, 480);
		frame.setTitle("SpaceBirds");
		frameRate(60);
		//draw the cursor as an image, because the native cursors don't support (real) transparency
		cursor = assetManager.loadImage("cursor.png", 32, 32);
		ambientSound = assetManager.loadClip("ambient.wav");
		ambientSound.loop(Clip.LOOP_CONTINUOUSLY);
		//noCursor();
		cursor(cursor);
		sceneManager.pushScene(SpaceBirdsLoadingScene.class);
	}
	
	@Override
	public void draw() {
		background(20, 20, 40);
		sceneManager.current().draw();
		//resetMatrix();
		//image(cursor, mouseX - cursor.width / 2, mouseY - cursor.height / 2);
	}
	
	@Override
	public void keyPressed() {
		if(key == CODED && keyCode == KeyEvent.VK_F5) {
			this.assetManager().clear();
		} else {
			super.keyPressed();
		}
		//prevent automatic close on ESC key -- see http://wiki.processing.org/w/Export_Info_and_Tips
		if(key == ESC) key = 0;
	}
	
	public static void main(String[] args) {
		init(SpaceBirds.class);
	}
}
