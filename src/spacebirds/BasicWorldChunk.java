package spacebirds;

import java.util.Random;

import spacebirds.base.Widget;

/**
 * World Chunk displaying 3 hurdles.
 * Hurdles can be GameHurdleWidget's or BlockedHurdleWidget's. Which one
 * is generated is decided randomly for each hurdle.
 * Chunks following this one can be BasicWorldChunks or PowerupWorldChunks
 * @author User
 *
 */

public class BasicWorldChunk extends GameWorldChunk {
	
	public static final int HURDLE_PASSAGE_SIZE = 200;
	
	private static Widget createHurdle(GameScene scene, Random rng, int w) {
		int h = scene.program().height;
		if(rng.nextDouble() < 0.3) {
			return new BlockedHurdleWidget(scene, w, rng.nextInt(h-HURDLE_PASSAGE_SIZE) + HURDLE_PASSAGE_SIZE/2, HURDLE_PASSAGE_SIZE);
		} else {
			return new GameHurdleWidget(scene, w, rng.nextInt(h-HURDLE_PASSAGE_SIZE) + HURDLE_PASSAGE_SIZE/2, HURDLE_PASSAGE_SIZE);
		}
	}
	
	protected static Widget[] createWorldChunk(GameScene scene, Random rng) {
		Widget hurdle1 = createHurdle(scene, rng, 0);
		Widget hurdle2 = createHurdle(scene, rng, scene.program().width / 3);
		Widget hurdle3 = createHurdle(scene, rng, scene.program().width / 3 * 2);
		return new Widget[] {hurdle1, hurdle2, hurdle3};
	}
	
	public BasicWorldChunk(GameScene scene, PlayerObject player, Random rng) {
		super(scene, player, rng, createWorldChunk(scene, rng));
	}

	@Override
	public void draw() {
		super.draw();
	}
	
	/**
	 * Return the next GameWorldChunk.
	 * After a BasicWorldChunk can follow a PowerupWorldChunk (prop. 0.3)
	 * or another BasicWorldChunk(0.7). 
	 */
	@Override
	public GameWorldChunk next() {
		double r = rng.nextDouble();
		if(r < 0.3) {
			return new PowerupWorldChunk(scene, player, rng);
		} else {
			return new BasicWorldChunk(scene, player, rng);
		}
	}

}
