package spacebirds;

import processing.core.PImage;
import spacebirds.base.*;

/**
 * A Hurdle with a passage somewhere in between the user has to fly through.
 */

public class GameHurdleWidget extends Widget {

	public static final float HURDLE_WIDTH = 64;
	public static final int DEFAULT_PASSAGE_SIZE = 150;
	
	protected double passagePosition;
	protected int passageSize;
	
	private PImage hurdleHeadTop, hurdleHeadBottom, hurdleTail;
		
	public GameHurdleWidget(Scene scene, double hurdlePosition, double passagePosition, int passageSize) {
		super(scene);
		this.x = (float) hurdlePosition;
		this.y = 0;
		this.passagePosition = passagePosition;
		this.passageSize = passageSize;
		
		this.hurdleHeadTop = p.assetManager().loadImage("hurdle_head_top.png", 0, 0);
		this.hurdleHeadBottom = p.assetManager().loadImage("hurdle_head_bottom.png", 0, 0);
		this.hurdleTail = p.assetManager().loadImage("hurdle_column.png", 0, 0);
	}
	
	@Override
	public void draw() {
		super.draw();
		
		int headTopPosition = (int) (y+ passagePosition - passageSize / 2 - hurdleHeadTop.height);
		int headBottomPosition = (int) (y+ passagePosition + passageSize / 2);
		int tailTopSize = headTopPosition + hurdleHeadTop.height / 2;
		int tailBottomPosition = headBottomPosition + hurdleHeadBottom.height / 2;
		int tailBottomSize = (int) (p.height - passagePosition - passageSize / 2 - hurdleHeadBottom.height / 2);
		
		p.image(hurdleTail, x, y,
				hurdleTail.width, tailTopSize,
				0, 0, hurdleTail.width, tailTopSize);
		
		p.image(hurdleTail, x, tailBottomPosition,
				hurdleTail.width, tailBottomSize,
				0, tailBottomPosition, hurdleTail.width, tailBottomSize);
		
		p.image(hurdleHeadTop, x, headTopPosition);
		p.image(hurdleHeadBottom, x, headBottomPosition);
		
	}
	
	@Override
	public Widget collidesImpl(Widget with) {
		Widget c = super.collidesImpl(with);
		if(c != null &&
				(with.getInnerY() < (y+passagePosition-passageSize/2) ||
				with.getInnerY() > (y+passagePosition+passageSize/2)))
		{
			return c;
		} else return null;
	}
	
	@Override
	public float getInnerWidth() {
		return HURDLE_WIDTH;
	}

	@Override
	public float getInnerHeight() {
		return p.height;
	}

}
