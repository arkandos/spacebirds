package spacebirds;

import javax.sound.sampled.Clip;

import processing.core.PImage;
import spacebirds.base.Scene;
import spacebirds.base.Widget;

/**
 * GameHurdle which is blocked at the beginning.
 * The player has to click on the hurdle to unblock it.
 */

public class BlockedHurdleWidget extends GameHurdleWidget {

	private boolean blocked = true;
	private PImage blockImage;
	private Clip explosionSound;
	
	public BlockedHurdleWidget(Scene scene, double hurdlePosition, double passagePosition, int passageSize) {
		super(scene, hurdlePosition, passagePosition, passageSize);
		blockImage = p.assetManager().loadImage("hurdle_block.png", 0, 0);
		explosionSound = p.assetManager().loadClip("kill_blocked.wav");
	}
	
	@Override
	public void draw() {
		super.draw();
		if(blocked) {
			p.image(blockImage, x, (float) (y + passagePosition - passageSize/2), blockImage.width, passageSize);
		}
	}
	
	@Override
	public void pause() {
		if(explosionSound.isRunning()) {
			explosionSound.stop();
		}
	}
	
	/**
	 * Collide with the whole bounding box if the hurdle is blocked.
	 */
	
	@Override
	public Widget collidesImpl(Widget with) {
		if(blocked) return innerBoundsCollide(with) ? with : null;
		else return super.collidesImpl(with);
	}
	
	/**
	 * unblock the hurdle if a mousePressed event is recieved; play an explosion sound.
	 */
	
	@Override
	public boolean mousePressed(int mouseButton, int mouseX, int mouseY) {
		blocked = false;
		explosionSound.setFramePosition(0);
		explosionSound.start();
		return true;
	}

}
