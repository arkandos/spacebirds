package spacebirds;

import spacebirds.base.Scene;
import spacebirds.scenes.LoadingScene;

/**
 * Loading scene to preload all the assets for the game.
 * Starts the StartMenuScene if finsihed.
 */

public class SpaceBirdsLoadingScene extends LoadingScene {

	@Override
	public String[] getImageResources() {
		return new String[] {
				//technical stuff
			"cursor.png",
			"font.png",
			"gauge.png",
				//background levels
			"background.jpg",
			"background_layer1.jpg",
			"background_layer2.png",
			"background_layer3.png",
			"overlay.png",
				//fancy headlines
			"spacebirds.png",
			"options.png",
			"pause.png",
				//player images
			"player.png",
			"player_ghost.png",
			"player_koopa.png",
			"player_up.png",
			"player_down.png",
			"player_hold.png",
				//powerups
			"powerup_none.png",
			"powerup_blitz.png",
			"powerup_ghost.png",
			"powerup_mario.png",
			"powerup_catapult.png",
				//gameworld elements
			"hurdle_column.png",
			"hurdle_head_top.png",
			"hurdle_head_bottom.png",
			"hurdle_block.png",
		};
	}

	@Override
	public String[] getClipResources() {
		return new String[] {
			"button_click.wav",
			"jump.wav",
			"kill_blocked.wav",
			"die.wav",
			"powerup.wav",
			"mario.wav",
			"ghost.wav",
		};
	}

	@Override
	public Class<? extends Scene> getNextScene() {
		return StartMenuScene.class;
	}

}
