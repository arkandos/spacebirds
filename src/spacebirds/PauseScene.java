package spacebirds;

import processing.core.PImage;
import spacebirds.base.*;
import spacebirds.scenes.WidgetScene;
import spacebirds.widgets.*;

/**
 * Scene displayed if the user pauses the game.
 * This expects a "screenshot" parameter in the Bundle to display as background and creating
 * the illusion of a transparent Scene and the game still being active in the (visual) background.
 * Allows to resume or end the game.
 */

public class PauseScene extends WidgetScene implements ButtonWidget.ClickListener {
	
	private PImage screenshot;
	private Widget resumeButton, rageQuitButton;
	
	@Override
	public void setup(BaseProgram parent, Bundle params) {
		super.setup(parent, params);
		
		screenshot = params.get("screenshot", null);
		
		this.add(new ImageWidget(this, "pause.png").alignCenter(0, 64, p.width, 64));
		
		rageQuitButton = this.add(new ButtonWidget(this, "Quit to Menu")
			.paddingToSize(p.width - 100, -1)
			.alignCenter(0, p.height - 70, p.width, p.height - 70));
		
		resumeButton = this.add(new ButtonWidget(this, "Resume")
			.paddingToSize(p.width - 100, -1)
			.alignCenter(0, rageQuitButton.getInnerY() - 70, p.width, rageQuitButton.getInnerY() - 70));
	}
	
	@Override
	public void draw() {
		if(screenshot != null) {
			p.tint(64);
			p.image(screenshot, 0, 0);
			p.noTint();
			//p.fill(20, 20, 40, 127);
			//p.rect(0, 0, p.width, p.height);
		}
		super.draw();
	}
	
	@Override
	public void buttonClicked(ButtonWidget button) {
		if(button == resumeButton) {
			p.sm().popScene();
		} else if(button == rageQuitButton) {
			p.sm().popScene();
			GameScene gs = (GameScene) p.sm().current();
			gs.endGame();
			//p.sm().popScene(StartMenuScene.class);
		}
	}
}
