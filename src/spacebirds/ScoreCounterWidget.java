package spacebirds;

import spacebirds.base.Scene;
import spacebirds.widgets.EightBitTextWidget;

/**
 * A EightBitTextWidget displaying a score as a integer number.
 */
public class ScoreCounterWidget extends EightBitTextWidget {

	private String prefix;
	private int score;
	
	private void updateLabel() {
		super.setLabel(prefix + String.valueOf(score));
	}
	
	public ScoreCounterWidget(Scene scene, String prefix, int initial) {
		super(scene, "");
		this.prefix = prefix;
		this.score = initial;
		this.updateLabel();
	}

	@Override
	public EightBitTextWidget setLabel(String text) {
		prefix = text;
		updateLabel();
		return this;
	}
	
	public int getScore() {
		return score;
	}
	/**
	 * convenience for changeValue(1)
	 */
	public final ScoreCounterWidget increment() {
		return this.changeValue(1);
	}
	/**
	 * convenience for changeValue(-1)
	 */
	public final ScoreCounterWidget decrement() {
		return this.changeValue(-1);
	}
	/**
	 * increase the score by delta
	 */
	public final ScoreCounterWidget changeValue(int delta) {
		return this.setScore(score + delta);
	}
	public ScoreCounterWidget setScore(int score) {
		this.score = score;
		updateLabel();
		return this;
	}

}
