package spacebirds.scenes;

import spacebirds.base.*;

/**
 * A Scene containing a WidgetManager and forwarding all events to it.
 */
public class WidgetScene extends Scene {
	
	protected WidgetManager widgetManager;
	
	public WidgetScene() {
		widgetManager = new WidgetManager();
	}
	
	public WidgetManager widgetManager() {
		return widgetManager;
	}
	
	public Widget add(Widget widget) {
		return widgetManager.add(widget);
	}
	public boolean remove(Widget widget) {
		return widgetManager.remove(widget);
	}
	public int size() {
		return widgetManager.size();
	}
	public Widget get(int i) {
		return widgetManager.get(i);
	}
	
	@Override
	public void setup(BaseProgram p, Bundle params) {
		super.setup(p, params);
		widgetManager.clear();
	}
	
	@Override
	public void pause() {
		widgetManager.pauseAll();
	}
	@Override
	public void resume() {
		widgetManager.resumeAll();
	}
	
	@Override
	public void draw() {
		widgetManager.drawAll();
	}
	
	@Override
	public void keyPressed(char key, int keyCode) {
		widgetManager.keyPressed(key, keyCode);
	}

	@Override
	public void keyReleased(char key, int keyCode) {
		widgetManager.keyReleased(key, keyCode);
	}

	@Override
	public void mousePressed(int mouseButton, int mouseX, int mouseY) {
		widgetManager.mousePressed(mouseButton, mouseX, mouseY);
	}

	@Override
	public void mouseReleased(int mouseButton, int mouseX, int mouseY) {
		widgetManager.mouseReleased(mouseButton, mouseX, mouseY);
	}

	@Override
	public void mouseMoved(int pmouseX, int pmouseY, int mouseX, int mouseY) {
		widgetManager.mouseMoved(pmouseX, pmouseY, mouseX, mouseY);
	}

	@Override
	public void mouseDragged(int mouseButton, int pmouseX, int pmouseY, int mouseX, int mouseY) {
		widgetManager.mouseDragged(mouseButton, pmouseX, pmouseY, mouseX, mouseY);
	}

	@Override
	public void mouseClicked(int mouseButton, int mouseX, int mouseY) {
		widgetManager.mouseClicked(mouseButton, mouseX, mouseY);
	}

	@Override
	public void mouseWheel(int delta, int mouseX, int mouseY) {
		widgetManager.mouseWheel(delta, mouseX, mouseY);
	}
	
}
