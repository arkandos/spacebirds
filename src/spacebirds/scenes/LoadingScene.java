package spacebirds.scenes;

import java.util.ArrayList;
import java.util.concurrent.*;

import javax.sound.sampled.Clip;

import processing.core.*;
import spacebirds.base.*;

/**
 * Preload assets by calling the load functions of the asset manager for a given array or assetNames asynchronly.
 * This scene pushes another one on front of it once everything is loaded.
 */
public abstract class LoadingScene extends Scene {
	
		
	private ArrayList<Future<?>> futureResourcesList;
	private ExecutorService threadPool;
	private String[] imageResources, clipResources;
	
	private static class ImageLoadingTask implements Callable<PImage> {
		private AssetManager assetManager;
		private String asset;
		public ImageLoadingTask(AssetManager am, String asset) {
			this.assetManager = am;
			this.asset = asset;
		}
		@Override
		public PImage call() throws Exception {
			return assetManager.loadImage(asset, 0, 0);
		}
		
	};
	private static class ClipLoadingTask implements Callable<Clip> {
		private AssetManager assetManager;
		private String asset;
		public ClipLoadingTask(AssetManager am, String asset) {
			this.assetManager = am;
			this.asset = asset;
		}
		@Override
		public Clip call() throws Exception {
			return assetManager.loadClip(asset);
		}
	}
	
	/**
	 * Image resources to load
	 */
	public abstract String[] getImageResources();
	/**
	 * Audio resources to load
	 */
	public abstract String[] getClipResources();
	/**
	 * Scene to push after this one.
	 */
	public abstract Class<? extends Scene> getNextScene();
	
	public LoadingScene() {
		imageResources = getImageResources();
		clipResources = getClipResources();
		futureResourcesList = new ArrayList<Future<?>>(imageResources.length);
		threadPool = Executors.newCachedThreadPool();
	}
	
	@Override
	public void setup(BaseProgram parent, Bundle params) {
		super.setup(parent, params);
		
		for(Future<?> futureImage : futureResourcesList) {
			futureImage.cancel(true);
		}
		futureResourcesList.clear();
		
		for(String resource : imageResources) {
			Future<PImage> futureImage = threadPool.submit(new ImageLoadingTask(parent.assetManager(), resource));
			futureResourcesList.add(futureImage);
		}
		for(String resource : clipResources) {
			Future<Clip> futureClip = threadPool.submit(new ClipLoadingTask(parent.assetManager(), resource));
			futureResourcesList.add(futureClip);
		}
	}
	
	private int getLoadingState() {
		int result = 0;
		for(Future<?> future : futureResourcesList) {
			if(future.isDone()) result += 1;
		}
		return result;
	}
	
	public void draw() {
		p.textAlign(PConstants.CENTER, PConstants.CENTER);
		
		String message = "<unused message>";
		int loadingState = this.getLoadingState();
		int endLoadingState = imageResources.length + clipResources.length;
		
		if(loadingState == endLoadingState) {
			message = "Press Space to continue...";
			p.sceneManager().pushScene(getNextScene());
		} else {
			message = "Loading... (" + loadingState + "/" + endLoadingState + ")";
		}
		
		p.text(message, 0, 0, p.width, p.height);
	}
	
	@Override
	public void keyPressed(char key, int keyCode) {
		if(key == ' ') {
			if(getLoadingState() == imageResources.length + clipResources.length) {
				p.sceneManager().pushScene(getNextScene());
			}
		}
	}
	@Override
	public void mouseClicked(int mouseButton, int mouseX, int mouseY) {
		if(mouseButton == LEFT) {
			if(getLoadingState() == imageResources.length + clipResources.length) {
				p.sceneManager().pushScene(getNextScene());
			}
		}
	}
}
