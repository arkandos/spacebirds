package spacebirds;

import java.util.Random;

import spacebirds.base.Widget;
import spacebirds.widgets.EightBitTextWidget;

/**
 * The First GameChunk created in every game.
 * This one is empty, and while it is active,
 * a helpText is displayed until the player activates gravity on the PlayerObject
 * (which usually happens by the first space press).
 * This Chunk is followed by FirstWorldChunks until the user starts playing,
 * after which it creates a BasicWorldChunk.
 */

public class FirstWorldChunk extends GameWorldChunk {
	
	private EightBitTextWidget helpText1, helpText2;

	public FirstWorldChunk(GameScene scene, PlayerObject player, int seed) {
		this(scene, player, new Random(seed));
	}
	
	public FirstWorldChunk(GameScene scene, PlayerObject player, Random rng) {
		super(scene, player, rng, new Widget[] {});
		
		helpText1 = new EightBitTextWidget(scene, "Press [Space] to jump, F to powerup!");
		helpText1.setFontSize(16)
			.setBackground(0, 0, 0, 128)
			.alignCenter(0, p.height - 200, p.width, p.height - 200)
			.paddingToSize(p.width, 80);
		
		helpText2 = new EightBitTextWidget(scene, "Try to click on blocked passages and powerups!");
		helpText2.setFontSize(16)
			.setBackground(0, 0, 0, 128)
			.alignCenter(0, p.height - 120, p.width, p.height - 120)
			.paddingToSize(p.width, 80);
	}

	public void draw() {
		super.draw();
		if(!player.gravityEnabled()) {
			helpText1.draw();
			helpText2.draw();
		}
	}
	
	public GameWorldChunk next() {
		if(player.gravityEnabled()) {
			return new BasicWorldChunk(scene, player, rng);
		} else {
			return new FirstWorldChunk(scene, player, rng);
		}
	}

}
