package spacebirds;

import processing.core.*;
import spacebirds.base.*;
import spacebirds.scenes.WidgetScene;
import spacebirds.widgets.*;

/**
 * Start Menu scene and the first "real" scene requiring interaction from the user.
 * Simply displays a button the end the game or to start it.
 */

public class StartMenuScene extends WidgetScene implements ButtonWidget.ClickListener {
	
	private PImage logo, background;
	private Widget newGameButton, quitButton;
	
	@Override
	public void setup(BaseProgram parent, Bundle params) {
		super.setup(parent, params);
		logo = parent.assetManager().loadImage("spacebirds.png", 0, 0);
		background = parent.assetManager().loadImage("background.jpg", parent.width, parent.height);
		
		quitButton = this.add(new ButtonWidget(this, "Quit Game")
			.paddingToSize(p.width - 100, -1)
			.alignCenter(0, p.height - 70, p.width, p.height - 70));
		
		newGameButton = this.add(new ButtonWidget(this, "New Game")
			.paddingToSize(p.width - 100, -1)
			.alignCenter(0, quitButton.getInnerY() - 70, p.width, quitButton.getInnerY() - 70));
		
		Widget impressumText = this.add(new EightBitTextWidget(this, "SpaceBirds by Joshua Reusch (c) 2014"));
		impressumText.setPosition(p.width - impressumText.getOuterWidth() - 10, p.height - impressumText.getOuterHeight() - 10);
	}
	
	@Override
	public void draw() {
		p.image(background, 0, 0);
		p.image(logo, 0, 30);
		super.draw();
	}

	@Override
	public void buttonClicked(ButtonWidget button) {
		if(button == newGameButton) {
			p.sm().pushScene(GameScene.class);
		} else if(button == quitButton) {
			p.exit();
		}
	}
}
